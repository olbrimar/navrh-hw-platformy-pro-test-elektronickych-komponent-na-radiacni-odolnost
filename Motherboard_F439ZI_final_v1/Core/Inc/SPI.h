/*
 * SPI.h
 *
 *  Created on: Dec 21, 2022
 *      Author: MartinOl
 */

#ifndef INC_SPI_H_
#define INC_SPI_H_

#include<stdio.h>

void SetChipSelect(int channel);
void WriteData (int channel, uint8_t *data, uint8_t *packet,  uint32_t addr, int lenght, uint8_t WRITE_INST, uint8_t WREN_INST);
void ReadData(int channel, uint8_t *receiveData, uint32_t addr, int lenght, uint8_t READ_INST, uint8_t WREN_INST);




#endif /* INC_SPI_H_ */
