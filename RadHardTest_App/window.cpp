#include "window.h"

#include <QApplication>
#include <QLabel>
#include <QWidget>
#include <QDebug>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QPushButton>
#include <QRegularExpression>
#include <QRegularExpressionValidator>
#include <QHostAddress>
#include <QFileDialog>
#include <QCheckBox>
#include <QMessageBox>
#include <QString>
#include <QFile>
#include <string.h>
#include <stdio.h>

#define TEMP_VAL 0x00
#define CURR_CH1_VAL 0x01
#define CURR_CH2_VAL 0x02
#define CURR_CH3_VAL 0x03
#define SUPPLY_CH1 0x04
#define SUPPLY_CH2 0x05
#define SUPPLY_CH3 0x06
#define WRITE_STAT 0x07
#define TEST_STAT 0x08
#define MEAS_CH1_RES 0x9
#define MEAS_CH2_RES 0xA
#define MEAS_CH3_RES 0xB
#define NUMBER_OF_BITES 0xC
#define LATCH_UP_TRSH_CH1 0xD
#define LATCH_UP_TRSH_CH2 0xE
#define LATCH_UP_TRSH_CH3 0xF
#define LATCH_UP_EVENT_CH1 0x10
#define LATCH_UP_EVENT_CH2 0x11
#define LATCH_UP_EVENT_CH3 0x12
#define ECC_CORRECTION_CH1 0x13
#define ECC_CORRECTION_CH2 0x14
#define ECC_CORRECTION_CH3 0x15

#define CMD_SET_SUPPLY 0x80
#define CMD_WRITE_CONST 0x81
#define CMD_WRITE_VAR 0x82
#define CMD_START_TEST 0x83
#define CMD_STOP_TEST 0x84
#define CMD_NUMBER_OF_BYTES 0x85
#define CMD_LATCHUP_LEVEL 0x86

// #define ADD_LOGO

uint32_t timeBase = 0;

uint32_t lastSumMisCH1 = 0;
uint32_t lastSumMisCH2 = 0;
uint32_t lastSumMisCH3 = 0;

window::window(QWidget *parent) : QWidget(parent)
{

    /*Nastaveni slotu pro timery*/
    timerGraph = new QTimer(this);
    timerWrite = new QTimer(this);
    connect(timerGraph, SIGNAL(timeout()), this, SLOT(timerGraphTimeout()));
    connect(timerWrite, SIGNAL(timeout()), this, SLOT(timerWriteTimeout()));

    /*deklarace skupin*/
    tcpGroup = new QGroupBox("TCP/IP");
    supplyGroup = new QGroupBox("Supply");
    tempGroup = new QGroupBox("Temperature");
    currentGroup = new QGroupBox("Current");
    sendCommand = new QGroupBox("Send a specific command");
    writeData = new QGroupBox("Write data");
    startTest = new QGroupBox("Start test");
    graphs = new QGroupBox("Graphs");

    /*Nastaveni fixni sirky jednotlivych groupBoxu*/
    tcpGroup->setFixedWidth(200);
    supplyGroup->setFixedWidth(200);
    tempGroup->setFixedSize(200, 50);
    currentGroup->setFixedWidth(200);
    sendCommand->setFixedWidth(200);
    writeData->setFixedWidth(200);
    startTest->setFixedWidth(200);

    /*Nastaveni GroupBoxu tcpGroup*/
    TCPcondis.setText("Connnect");

    connect(&TCPcondis, SIGNAL(clicked()), this, SLOT(clickedConDis()));
    connect(tcp.socket, SIGNAL(disconnected()), this, SLOT(changePushText()));

    vbox_tcp.addWidget(&TCPcondis);
    tcpGroup->setLayout(&vbox_tcp);

    /*Nastaveni tlacitka refresh*/
    regRef.setText("Refresh");
    regRef.setDisabled(true);
    connect(&regRef, SIGNAL(clicked()), this, SLOT(clickedRef()));

    /*Nastaveni GroupBoxu supplyGroup*/
    CH1.setText("channel 1");
    CH2.setText("channel 2");
    CH3.setText("channel 3");

    CH1.setStyleSheet("margin-left:50%; margin-right:50%");
    CH2.setStyleSheet("margin-left:50%; margin-right:50%");
    CH3.setStyleSheet("margin-left:50%; margin-right:50%");

    supplyGroup->setDisabled(true);

    vbox_supply.addWidget(&CH1);
    vbox_supply.addWidget(&CH2);
    vbox_supply.addWidget(&CH3);
    supplyGroup->setLayout(&vbox_supply);

    connect(&CH1, SIGNAL(clicked(bool)), this, SLOT(stateChangedCH1(bool)));
    connect(&CH2, SIGNAL(clicked(bool)), this, SLOT(stateChangedCH2(bool)));
    connect(&CH3, SIGNAL(clicked(bool)), this, SLOT(stateChangedCH3(bool)));

    /*Nastaveni GroupBoxu tempGroup*/
    tempLabel.setText("Temperature: ");
    temp.setText(" - ");
    tempUnit.setText("°C");

    hbox_temp.addWidget(&tempLabel);
    hbox_temp.addWidget(&temp);
    hbox_temp.addWidget(&tempUnit);
    tempGroup->setLayout(&hbox_temp);

    /*Nastaveni GroupBoxu currentGroup*/

    CH1currLabel.setText("CH1: ");
    CH1curr.setText(" - ");
    CH2currLabel.setText("CH2: ");
    CH2curr.setText(" - ");
    CH3currLabel.setText("CH3: ");
    CH3curr.setText(" - ");
    currUnitCH1.setText("mA");
    currUnitCH2.setText("mA");
    currUnitCH3.setText("mA");

    trshLabelCH1.setText("Latch-up treshold: ");
    trshUnitCH1.setText("mA");
    trshLabelCH2.setText("Latch-up treshold: ");
    trshUnitCH2.setText("mA");
    trshLabelCH3.setText("Latch-up treshold: ");
    trshUnitCH3.setText("mA");

    latchupSliderCH1 = new QSlider(Qt::Horizontal);
    latchupSliderCH2 = new QSlider(Qt::Horizontal);
    latchupSliderCH3 = new QSlider(Qt::Horizontal);

    latchupSliderCH1->setRange(0, 500);
    latchupSliderCH1->setValue(250);

    latchupSliderCH2->setRange(0, 500);
    latchupSliderCH2->setValue(250);

    latchupSliderCH3->setRange(0, 500);
    latchupSliderCH3->setValue(250);
    trshValueCH1.setText(QString::number(latchupSliderCH1->value()));
    trshValueCH2.setText(QString::number(latchupSliderCH2->value()));
    trshValueCH3.setText(QString::number(latchupSliderCH1->value()));

    connect(latchupSliderCH1, SIGNAL(valueChanged(int)), this, SLOT(sliderChangedCH1()));
    connect(latchupSliderCH2, SIGNAL(valueChanged(int)), this, SLOT(sliderChangedCH2()));
    connect(latchupSliderCH3, SIGNAL(valueChanged(int)), this, SLOT(sliderChangedCH3()));

    hbox_curr_CH1.addWidget(&CH1currLabel);
    hbox_curr_CH1.addWidget(&CH1curr);
    hbox_curr_CH1.addWidget(&currUnitCH1);
    hbox_curr_CH2.addWidget(&CH2currLabel);
    hbox_curr_CH2.addWidget(&CH2curr);
    hbox_curr_CH2.addWidget(&currUnitCH2);
    hbox_curr_CH3.addWidget(&CH3currLabel);
    hbox_curr_CH3.addWidget(&CH3curr);
    hbox_curr_CH3.addWidget(&currUnitCH3);

    hbox_trshCH1.addWidget(&trshLabelCH1);
    hbox_trshCH1.addWidget(&trshValueCH1);
    hbox_trshCH1.addWidget(&trshUnitCH1);

    hbox_trshCH2.addWidget(&trshLabelCH2);
    hbox_trshCH2.addWidget(&trshValueCH2);
    hbox_trshCH2.addWidget(&trshUnitCH2);

    hbox_trshCH3.addWidget(&trshLabelCH3);
    hbox_trshCH3.addWidget(&trshValueCH3);
    hbox_trshCH3.addWidget(&trshUnitCH3);

    vbox_curr.addLayout(&hbox_curr_CH1);
    vbox_curr.addLayout(&hbox_trshCH1);
    vbox_curr.addWidget(latchupSliderCH1);
    vbox_curr.addLayout(&hbox_curr_CH2);
    vbox_curr.addLayout(&hbox_trshCH2);
    vbox_curr.addWidget(latchupSliderCH2);
    vbox_curr.addLayout(&hbox_curr_CH3);
    vbox_curr.addLayout(&hbox_trshCH3);
    vbox_curr.addWidget(latchupSliderCH3);

    currentGroup->setLayout(&vbox_curr);
    currentGroup->setDisabled(true);

    /*Nastaveni GroupBoxu writeData*/
    setConst.setText("0x55");
    constValWrite.setText("Write");
    varValWrite.setText("Write");
    writeDataLabel.setText("Variable values");
    constValWrite.setFixedWidth(75);
    varValWrite.setFixedWidth(75);

    connect(&constValWrite, SIGNAL(clicked()), this, SLOT(clickedConstValWrite()));
    connect(&varValWrite, SIGNAL(clicked()), this, SLOT(clickedVarWrite()));

    writeData->setDisabled(true);

    hbox_writeConstData.addWidget(&setConst);
    hbox_writeConstData.addWidget(&constValWrite);
    hbox_writeVarData.addWidget(&writeDataLabel);
    hbox_writeVarData.addWidget(&varValWrite);
    vbox_writeData.addLayout(&hbox_writeConstData);
    vbox_writeData.addLayout(&hbox_writeVarData);
    writeData->setLayout(&vbox_writeData);

    /*nastaveni groupboxu startTest*/
    startErrTest.setText("Start");
    stopErrTest.setText("Stop");
    numberOfMeasLabel.setText("Number of Meas.:");
    numberOfMeas.setText("1");
    numberOfMeas.setInputMask("9999");
    filenamePath.setText("Add path");
    RWCheckBox.setText("R-W");
    varTestCheckBox.setText("Var. test");
    ECCBox.setText("ECC");
    varTestCheckBox.setDisabled(true);

    connect(&startErrTest, SIGNAL(clicked()), this, SLOT(clickedStartErrTest()));
    connect(&stopErrTest, SIGNAL(clicked()), this, SLOT(clickedStopErrTest()));
    connect(&filenamePath, SIGNAL(clicked()), this, SLOT(clickedFilenamePath()));
    connect(&RWCheckBox, SIGNAL(clicked(bool)), this, SLOT(stateChangedRW(bool)));
    connect(&varTestCheckBox, SIGNAL(clicked(bool)), this, SLOT(clickedVarTestCheckBox(bool)));

    startTest->setDisabled(true);

    hbox_startTestNumberOfMeas.addWidget(&numberOfMeasLabel);
    hbox_startTestNumberOfMeas.addWidget(&numberOfMeas);
    hbox_startTestPath.addWidget(&filenameLineEdit);
    hbox_startTestPath.addWidget(&filenamePath);
    hbox_varTest.addWidget(&startErrTest);
    hbox_varTest.addWidget(&stopErrTest);
    hbox_startTestCheckBox.addWidget(&RWCheckBox);
    hbox_startTestCheckBox.addWidget(&varTestCheckBox);
    hbox_startTestCheckBox.addWidget(&ECCBox);

    vbox_startTest.addLayout(&hbox_startTestNumberOfMeas);
    vbox_startTest.addLayout(&hbox_startTestPath);
    vbox_startTest.addLayout(&hbox_startTestCheckBox);
    vbox_startTest.addLayout(&hbox_varTest);
    startTest->setLayout(&vbox_startTest);

    /*Nastaveni GroupBoxu sendCommand*/
    sendCommandBut.setText("Send");

    connect(&sendCommandBut, SIGNAL(clicked()), this, SLOT(clickedSend()));

    sendCommand->setDisabled(true);

    hbox_sendCommand.addWidget(&sendCommandLine);
    hbox_sendCommand.addWidget(&sendCommandBut);
    sendCommand->setLayout(&hbox_sendCommand);

/*Pridani loga UTEFu*/
#ifdef ADD_LOGO
    QPixmap picture("C:/Users/Marti/Documents/VS_code/BP/TCP_packet_test/pictures/logo_cvut.svg");
    pictureLabel.setPixmap(picture.scaled(100, 50, Qt::KeepAspectRatio, Qt::SmoothTransformation));
    pictureLabel.setAlignment(Qt::AlignCenter);
#endif

    graphs->setDisabled(true);
    hbox_errBar.addWidget(graph.plotMis);
    hbox_errBar.addWidget(graph.plotMisECC);
    vbox_graph.addLayout(&hbox_errBar);
    vbox_graph.addWidget(graph.plotDelta);
    vbox_graph.addWidget(graph.plotCurr);
    vbox_graph.addLayout(&hbox_saveBut);
    graphs->setLayout(&vbox_graph);

    /*Pridani jed. skupin do layoutu*/
    vbox_right.addWidget(tcpGroup);
    vbox_right.addWidget(&regRef);
    vbox_right.addWidget(supplyGroup);
    vbox_right.addWidget(tempGroup);
    vbox_right.addWidget(currentGroup);
    vbox_right.addWidget(writeData);
    vbox_right.addWidget(startTest);
    vbox_right.addWidget(sendCommand);
#ifdef ADD_LOGO
    vbox_right.addWidget(&pictureLabel);
#endif
    vbox_left.addWidget(graphs);

    hbox.addLayout(&vbox_left);
    hbox.addLayout(&vbox_right);
    setLayout(&hbox);
}

void window::clickedConDis()
{
    if (!tcp.socket->state())
    {
        tcp.connection("192.168.0.111", 7);

        if (tcp.socket->waitForConnected(500))
        {
            TCPcondis.setText("Disconnect");

            regRef.setDisabled(false);
            supplyGroup->setDisabled(false);
            currentGroup->setDisabled(false);
            writeData->setDisabled(false);
            sendCommand->setDisabled(false);
            startTest->setDisabled(false);
            startErrTest.setDisabled(false);
            stopErrTest.setDisabled(true);

            tcp.loadReg();
            CH1.setChecked(tcp.reg[SUPPLY_CH1]);
            CH2.setChecked(tcp.reg[SUPPLY_CH2]);
            CH3.setChecked(tcp.reg[SUPPLY_CH3]);
            float val;
            memcpy(&val, &tcp.reg[CURR_CH1_VAL], sizeof(uint32_t));
            CH1curr.setText(QString::number(val, 10, 3));
            memcpy(&val, &tcp.reg[CURR_CH2_VAL], sizeof(uint32_t));
            CH2curr.setText(QString::number(val, 10, 3));
            memcpy(&val, &tcp.reg[CURR_CH3_VAL], sizeof(uint32_t));
            CH3curr.setText(QString::number(val, 10, 3));
            memcpy(&val, &tcp.reg[TEMP_VAL], sizeof(uint32_t));
            temp.setText(QString::number(val, 10, 3));

            
            latchupSliderCH1->setValue(tcp.reg[LATCH_UP_TRSH_CH1]);
            latchupSliderCH2->setValue(tcp.reg[LATCH_UP_TRSH_CH2]);
            latchupSliderCH3->setValue(tcp.reg[LATCH_UP_TRSH_CH3]);
        }
        else
        {
            QMessageBox::warning(this, tr("Connection error"), tr("Unable to connect!!"), QMessageBox::Cancel);
        }
    }

    else if (tcp.socket->state())
    {
        tcp.socket->disconnectFromHost();

        TCPcondis.setText("Connect");

        regRef.setDisabled(true);
        supplyGroup->setDisabled(true);
        currentGroup->setDisabled(true);
        writeData->setDisabled(true);
        sendCommand->setDisabled(true);
        startTest->setDisabled(true);

        if (timerGraph->isActive() == 1)
            timerGraph->stop();
    }
}

void window::changePushText()
{
    if (timerGraph->isActive() == 1)
    {
        timerGraph->stop();

        graph.yMisData[0] = 0;
        graph.yMisData[1] = 0;
        graph.yMisData[2] = 0;
        graph.misBar->setData(graph.xMisData, graph.yMisData);
        graph.plotMis->yAxis->setRange(-1, *std::max_element(graph.yMisData.begin(), graph.yMisData.end()) + 1);
        graph.plotMis->replot();

        graph.yMisDataECC[0] = 0;
        graph.yMisDataECC[1] = 0;
        graph.yMisDataECC[2] = 0;
        graph.misBarECC->setData(graph.xMisDataECC, graph.yMisDataECC);
        graph.plotMisECC->yAxis->setRange(-1, *std::max_element(graph.yMisDataECC.begin(), graph.yMisDataECC.end()) + 1);
        graph.plotMisECC->replot();

        timeBase = 0;
        graph.xCurrData.clear();
        graph.yCurrDataCH1.clear();
        graph.yCurrDataCH2.clear();
        graph.yCurrDataCH3.clear();
        graph.xDeltaData.clear();
        graph.yDeltaDataCH1.clear();
        graph.yCurrDataCH2.clear();
        graph.yCurrDataCH3.clear();
        graph.yDeltaDataCH1.clear();
        graph.yDeltaDataCH2.clear();
        graph.yDeltaDataCH3.clear();
        graph.yLatchUpCH1.clear();
        graph.yLatchUpCH2.clear();
        graph.yLatchUpCH3.clear();
        graph.plotCurr->replot();
        graph.plotDelta->replot();
    }

    TCPcondis.setText("Connect");
    regRef.setDisabled(true);
    supplyGroup->setDisabled(true);
    currentGroup->setDisabled(true);
    writeData->setDisabled(true);
    sendCommand->setDisabled(true);
    startTest->setDisabled(true);
    currentGroup->setDisabled(true);
    qDebug() << tcp.socket->state();
}

void window::clickedRef()
{
    tcp.loadReg();
    CH1.setChecked(tcp.reg[SUPPLY_CH1]);
    CH2.setChecked(tcp.reg[SUPPLY_CH2]);
    CH3.setChecked(tcp.reg[SUPPLY_CH3]);
    float val;
    memcpy(&val, &tcp.reg[CURR_CH1_VAL], sizeof(uint32_t));
    CH1curr.setText(QString::number(val, 10, 3));
    memcpy(&val, &tcp.reg[CURR_CH2_VAL], sizeof(uint32_t));
    CH2curr.setText(QString::number(val, 10, 3));
    memcpy(&val, &tcp.reg[CURR_CH3_VAL], sizeof(uint32_t));
    CH3curr.setText(QString::number(val, 10, 3));
    memcpy(&val, &tcp.reg[TEMP_VAL], sizeof(uint32_t));
    temp.setText(QString::number(val, 10, 3));

    latchupSliderCH1->setValue(tcp.reg[LATCH_UP_TRSH_CH1]);
    latchupSliderCH2->setValue(tcp.reg[LATCH_UP_TRSH_CH2]);
    latchupSliderCH3->setValue(tcp.reg[LATCH_UP_TRSH_CH3]);
}

void window::stateChangedCH1(bool)
{
    tcp.reg[SUPPLY_CH1] = (bool)CH1.checkState();
    tcp.command_set(0x80, 1, tcp.reg[SUPPLY_CH1]);
}

void window::stateChangedCH2(bool)
{
    tcp.reg[SUPPLY_CH2] = (bool)CH2.checkState();
    tcp.command_set(0x80, 2, tcp.reg[SUPPLY_CH2]);
}

void window::stateChangedCH3(bool)
{
    tcp.reg[SUPPLY_CH3] = (bool)CH3.checkState();
    tcp.command_set(0x80, 3, tcp.reg[SUPPLY_CH3]);
}

void window::clickedSend()
{
    QByteArray send = sendCommandLine.text().toUtf8();
    qDebug() << send;
    tcp.socket->write(send);
    tcp.socket->waitForBytesWritten();
    sendCommandLine.clear();
    tcp.loadReg();
    CH1.setChecked(tcp.reg[SUPPLY_CH1]);
    CH2.setChecked(tcp.reg[SUPPLY_CH2]);
    CH3.setChecked(tcp.reg[SUPPLY_CH3]);
    float val;
    memcpy(&val, &tcp.reg[CURR_CH1_VAL], sizeof(uint32_t));
    CH1curr.setText(QString::number(val, 10, 3));
    memcpy(&val, &tcp.reg[CURR_CH2_VAL], sizeof(uint32_t));
    CH2curr.setText(QString::number(val, 10, 3));
    memcpy(&val, &tcp.reg[CURR_CH3_VAL], sizeof(uint32_t));
    CH3curr.setText(QString::number(val, 10, 3));
    memcpy(&val, &tcp.reg[TEMP_VAL], sizeof(uint32_t));
    temp.setText(QString::number(val, 10, 3));
}

void window::clickedConstValWrite()
{
    bool status = false;
    tcp.command_set(CMD_WRITE_CONST, 0, setConst.text().toUInt(&status, 16));
    timerWrite->start(writeCheckInterval);
    writeVarStat = false;

    tcpGroup->setDisabled(true);
    supplyGroup->setDisabled(true);
    currentGroup->setDisabled(true);
    currentGroup->setDisabled(true);
    sendCommand->setDisabled(true);
    writeData->setDisabled(true);
    startTest->setDisabled(true);
    graphs->setDisabled(true);
    regRef.setDisabled(true);
}

void window::clickedVarWrite()
{

    tcp.command_set(CMD_WRITE_VAR, 0, 0);
    timerWrite->start(writeCheckInterval);

    tcpGroup->setDisabled(true);
    supplyGroup->setDisabled(true);
    currentGroup->setDisabled(true);
    sendCommand->setDisabled(true);
    writeData->setDisabled(true);
    startTest->setDisabled(true);
    graphs->setDisabled(true);
    regRef.setDisabled(true);
    currentGroup->setDisabled(true);
    writeVarStat = true;
}

void window::timerWriteTimeout()
{
    uint32_t write_status = tcp.command_get(WRITE_STAT);
    if (write_status == 0 && !(bool)RWCheckBox.isChecked())
    {
        timerWrite->stop();
        QMessageBox::information(this, tr("Radiation hardness test"), tr("The data has been written!!"));

        tcpGroup->setDisabled(false);
        supplyGroup->setDisabled(false);
        currentGroup->setDisabled(false);
        sendCommand->setDisabled(false);
        writeData->setDisabled(false);
        startTest->setDisabled(false);
        startErrTest.setDisabled(false);
        stopErrTest.setDisabled(true);
        regRef.setDisabled(false);
    }
    else if (write_status == 0)
    { /*dobehnuty zapis v rezimu R-W*/

        timerWrite->stop();
        tcp.loadReg();
        timeBase = timeBase + writeCheckInterval / 1000;

        if (graph.draw_curr(timeBase, tcp.reg[CURR_CH1_VAL], tcp.reg[CURR_CH2_VAL], tcp.reg[CURR_CH3_VAL], tcp.reg[LATCH_UP_EVENT_CH1], tcp.reg[LATCH_UP_EVENT_CH2], tcp.reg[LATCH_UP_EVENT_CH3]))
        {
            QMessageBox::information(this, tr("Radiation hardness test"), tr("Latch Up event!"));
            timerWrite->stop();
            regRef.setDisabled(false);
            supplyGroup->setDisabled(false);
            currentGroup->setDisabled(false);
            writeData->setDisabled(false);
            sendCommand->setDisabled(false);
            startErrTest.setDisabled(false);
            stopErrTest.setDisabled(true);
            numberOfMeas.setDisabled(false);
            filenameLineEdit.setDisabled(false);
            filenamePath.setDisabled(false);
            graphs->setDisabled(false);
            RWCheckBox.setDisabled(false);
            ECCBox.setDisabled(false);
            VarTestBox((bool)RWCheckBox.checkState());

            SaveErrToTxt();
            SaveDeltaToTxt();
            SaveCurrToTxt();
            if ((bool)ECCBox.isChecked() == true)
                SaveECCToTxt();
            fileNumber++;

            return;
        }

        tcp.command_set(CMD_START_TEST, 0, 0);
        timerGraph->start(timerTime);
    }
    else if (write_status == 1 && (bool)RWCheckBox.isChecked())
    { /*Rezim W-R vypis proudu*/

        tcp.loadReg();
        timeBase = timeBase + writeCheckInterval / 1000;

        if (graph.draw_curr(timeBase, tcp.reg[CURR_CH1_VAL], tcp.reg[CURR_CH2_VAL], tcp.reg[CURR_CH3_VAL], tcp.reg[LATCH_UP_EVENT_CH1], tcp.reg[LATCH_UP_EVENT_CH2], tcp.reg[LATCH_UP_EVENT_CH3]))
        {
            timerGraph->stop();
            QMessageBox::information(this, tr("Radiation hardness test"), tr("Latch Up event!"));
            timerGraph->stop();
            regRef.setDisabled(false);
            supplyGroup->setDisabled(false);
            currentGroup->setDisabled(false);
            writeData->setDisabled(false);
            sendCommand->setDisabled(false);
            startErrTest.setDisabled(false);
            stopErrTest.setDisabled(true);
            numberOfMeas.setDisabled(false);
            filenameLineEdit.setDisabled(false);
            filenamePath.setDisabled(false);
            graphs->setDisabled(false);
            RWCheckBox.setDisabled(false);
            ECCBox.setDisabled(false);
            VarTestBox((bool)RWCheckBox.checkState());

            SaveErrToTxt();
            SaveDeltaToTxt();
            SaveCurrToTxt();
            if ((bool)ECCBox.isChecked() == true)
                SaveECCToTxt();
            fileNumber++;

            return;
        }
    }
}

void window::timerGraphTimeout()
{
    tcp.loadReg();
    timeBase = timeBase + timerTime / 1000;

    // Vykresleni souctu chyb
    graph.yMisData[0] = (double)tcp.reg[MEAS_CH1_RES];
    graph.yMisData[1] = (double)(tcp.reg[MEAS_CH2_RES]);
    graph.yMisData[2] = (double)(tcp.reg[MEAS_CH3_RES]);
    graph.misBar->setData(graph.xMisData, graph.yMisData);
    graph.plotMis->yAxis->setRange(-1, *std::max_element(graph.yMisData.begin(), graph.yMisData.end()) + 1);
    graph.plotMis->replot();
    if ((bool)ECCBox.checkState() == true)
    {
        qDebug() << "Correction graph";
        graph.yMisDataECC[0] = (double)tcp.command_get(ECC_CORRECTION_CH1);
        graph.yMisDataECC[1] = (double)tcp.command_get(ECC_CORRECTION_CH2);
        graph.yMisDataECC[2] = (double)tcp.command_get(ECC_CORRECTION_CH3);
        graph.misBarECC->setData(graph.xMisDataECC, graph.yMisDataECC);
        graph.plotMisECC->yAxis->setRange(-1, *std::max_element(graph.yMisDataECC.begin(), graph.yMisDataECC.end()) + 1);
        graph.plotMisECC->replot();
    }

    // vykresleni grafu s deltou chyb
    graph.draw_delta(timeBase, (tcp.reg[MEAS_CH1_RES] - lastSumMisCH1), (tcp.reg[MEAS_CH2_RES] - lastSumMisCH2), (tcp.reg[MEAS_CH3_RES] - lastSumMisCH3));
    lastSumMisCH1 = tcp.reg[MEAS_CH1_RES];
    lastSumMisCH2 = tcp.reg[MEAS_CH2_RES];
    lastSumMisCH3 = tcp.reg[MEAS_CH3_RES];

    // Vykresleni grafu s proudem
    if (graph.draw_curr(timeBase, tcp.reg[CURR_CH1_VAL], tcp.reg[CURR_CH2_VAL], tcp.reg[CURR_CH3_VAL], tcp.reg[LATCH_UP_EVENT_CH1], tcp.reg[LATCH_UP_EVENT_CH2], tcp.reg[LATCH_UP_EVENT_CH3]))
    {
        timerGraph->stop();
        QMessageBox::information(this, tr("Radiation hardness test"), tr("Latch Up event!"));
        timerGraph->stop();
        regRef.setDisabled(false);
        supplyGroup->setDisabled(false);
        currentGroup->setDisabled(false);
        writeData->setDisabled(false);
        sendCommand->setDisabled(false);
        startErrTest.setDisabled(false);
        stopErrTest.setDisabled(true);
        numberOfMeas.setDisabled(false);
        filenameLineEdit.setDisabled(false);
        filenamePath.setDisabled(false);
        graphs->setDisabled(false);
        RWCheckBox.setDisabled(false);
        ECCBox.setDisabled(false);
        VarTestBox((bool)RWCheckBox.checkState());

        SaveErrToTxt();
        SaveDeltaToTxt();
        SaveCurrToTxt();
        if ((bool)ECCBox.isChecked() == true)
            SaveECCToTxt();
        fileNumber++;

        return;
    }

    /*Nastaveni opakovaneho mereni*/
    if (tcp.reg[TEST_STAT] == 0)
    {
        int meas = numberOfMeas.text().toInt();
        if ((meas - 1) == 0)
        {
            timerGraph->stop();
            regRef.setDisabled(false);
            supplyGroup->setDisabled(false);
            currentGroup->setDisabled(false);
            writeData->setDisabled(false);
            sendCommand->setDisabled(false);
            startErrTest.setDisabled(false);
            stopErrTest.setDisabled(true);
            numberOfMeas.setDisabled(false);
            filenameLineEdit.setDisabled(false);
            filenamePath.setDisabled(false);
            graphs->setDisabled(false);
            RWCheckBox.setDisabled(false);
            ECCBox.setDisabled(false);
            VarTestBox((bool)RWCheckBox.checkState());

            /*volani funkci na zapis do souboru*/
            SaveErrToTxt();
            SaveDeltaToTxt();
            SaveCurrToTxt();
            if ((bool)ECCBox.isChecked() == true)
                SaveECCToTxt();
        }
        else
        {
            if ((bool)RWCheckBox.checkState())
            {
                bool status = false;
                if ((bool)varTestCheckBox.checkState())
                    tcp.command_set(CMD_WRITE_VAR, 0, 0);
                else
                {
                    tcp.command_set(CMD_WRITE_CONST, 0, setConst.text().toUInt(&status, 16));
                }

                timerGraph->stop();
                timerWrite->start(writeCheckInterval);
            }
            else
            {
                tcp.command_set(CMD_START_TEST, 0, 0);
            }

            numberOfMeas.setText(QString::number(--meas));
            lastSumMisCH1 = 0;
            lastSumMisCH2 = 0;
            lastSumMisCH3 = 0;

            /*volani funkci na zapis do souboru*/
            SaveErrToTxt();
            SaveDeltaToTxt();
            SaveCurrToTxt();
            if ((bool)ECCBox.isChecked() == true)
                SaveECCToTxt();
            fileNumber++;

            // Nastaveni grafu s chyby
            graph.yMisData[0] = 0;
            graph.yMisData[1] = 0;
            graph.yMisData[2] = 0;
            graph.misBar->setData(graph.xMisData, graph.yMisData);
            graph.plotMis->yAxis->setRange(-1, *std::max_element(graph.yMisData.begin(), graph.yMisData.end()) + 1);
            graph.plotMis->replot();

            if ((bool)ECCBox.checkState() == true)
            {
                graph.yMisDataECC[0] = 0;
                graph.yMisDataECC[1] = 0;
                graph.yMisDataECC[2] = 0;
                graph.misBarECC->setData(graph.xMisDataECC, graph.yMisDataECC);
                graph.plotMisECC->yAxis->setRange(-1, *std::max_element(graph.yMisDataECC.begin(), graph.yMisDataECC.end()) + 1);
                graph.plotMisECC->replot();
            }

            // Vyprazdneni data
            timeBase = 0;
            graph.xCurrData.clear();
            graph.yCurrDataCH1.clear();
            graph.yCurrDataCH2.clear();
            graph.yCurrDataCH3.clear();
            graph.xDeltaData.clear();
            graph.yDeltaDataCH1.clear();
            graph.yCurrDataCH2.clear();
            graph.yCurrDataCH3.clear();
            graph.yDeltaDataCH1.clear();
            graph.yDeltaDataCH2.clear();
            graph.yDeltaDataCH3.clear();
            graph.yLatchUpCH1.clear();
            graph.yLatchUpCH2.clear();
            graph.yLatchUpCH3.clear();

            bool num = graph.draw_curr(timeBase, tcp.reg[CURR_CH1_VAL], tcp.reg[CURR_CH2_VAL], tcp.reg[CURR_CH3_VAL], tcp.reg[LATCH_UP_EVENT_CH1], tcp.reg[LATCH_UP_EVENT_CH2], tcp.reg[LATCH_UP_EVENT_CH3]);
        }
    }
}

void window::clickedStartErrTest()
{
    bool status = false;
    tcp.command_set(CMD_LATCHUP_LEVEL, 1, latchupSliderCH1->value());
    QThread::msleep(500);
    tcp.command_set(CMD_LATCHUP_LEVEL, 2, latchupSliderCH2->value());
    QThread::msleep(500);
    tcp.command_set(CMD_LATCHUP_LEVEL, 3, latchupSliderCH3->value());
    QThread::msleep(500);

    if (filenameLineEdit.text() == "")
    {
        QMessageBox::warning(this, tr("Path error!"), tr("Path is empty!!"), QMessageBox::Cancel);
        return;
    }
    else if (RWCheckBox.isChecked())
    {
        /*Pusteni testu R-W*/
        bool status = false;
        if ((bool)varTestCheckBox.checkState())
            tcp.command_set(CMD_WRITE_VAR, 0, 0);
        else
        {
            tcp.command_set(CMD_WRITE_CONST, 0, setConst.text().toUInt(&status, 16));
        }
        timerWrite->start(writeCheckInterval);
    }
    else
    {
        tcp.command_set(CMD_START_TEST, 0, 0);
    
        timerGraph->start(timerTime);
    }

    /*Zsednuti kolonek*/
    regRef.setDisabled(true);
    supplyGroup->setDisabled(true);
    writeData->setDisabled(true);
    sendCommand->setDisabled(true);
    startTest->setDisabled(false);
    startErrTest.setDisabled(true);
    stopErrTest.setDisabled(false);
    numberOfMeas.setDisabled(true);
    filenameLineEdit.setDisabled(true);
    filenamePath.setDisabled(true);
    RWCheckBox.setDisabled(true);
    ECCBox.setDisabled(true);
    varTestCheckBox.setDisabled(true);
    graphs->setDisabled(true);
    currentGroup->setDisabled(true);

    qApp->processEvents();
    // Nastaveni grafu s chyby
    graph.yMisData[0] = 0;
    graph.yMisData[1] = 0;
    graph.yMisData[2] = 0;
    graph.misBar->setData(graph.xMisData, graph.yMisData);
    graph.plotMis->yAxis->setRange(-1, *std::max_element(graph.yMisData.begin(), graph.yMisData.end()) + 1);
    graph.plotMis->replot();

    graph.yMisDataECC[0] = 0;
    graph.yMisDataECC[1] = 0;
    graph.yMisDataECC[2] = 0;
    graph.misBarECC->setData(graph.xMisDataECC, graph.yMisDataECC);
    graph.plotMisECC->yAxis->setRange(-1, *std::max_element(graph.yMisDataECC.begin(), graph.yMisDataECC.end()) + 1);
    graph.plotMisECC->replot();

    qApp->processEvents();

    // Vyprazdneni data
    timeBase = 0;
    graph.xCurrData.clear();
    graph.yCurrDataCH1.clear();
    graph.yCurrDataCH2.clear();
    graph.yCurrDataCH3.clear();
    graph.xDeltaData.clear();
    graph.yDeltaDataCH1.clear();
    graph.yCurrDataCH2.clear();
    graph.yCurrDataCH3.clear();
    graph.yDeltaDataCH1.clear();
    graph.yDeltaDataCH2.clear();
    graph.yDeltaDataCH3.clear();
    graph.yLatchUpCH1.clear();
    graph.yLatchUpCH2.clear();
    graph.yLatchUpCH3.clear();

    // nacteni registru
    tcp.loadReg();

    qApp->processEvents();

    // Vypis do grafu s proudem
    bool num = graph.draw_curr(timeBase, tcp.reg[CURR_CH1_VAL], tcp.reg[CURR_CH2_VAL], tcp.reg[CURR_CH3_VAL], tcp.reg[LATCH_UP_EVENT_CH1], tcp.reg[LATCH_UP_EVENT_CH2], tcp.reg[LATCH_UP_EVENT_CH3]);

    // Vypis grafu s deltou
    lastSumMisCH1 = 0;
    lastSumMisCH2 = 0;
    lastSumMisCH3 = 0;
    graph.draw_delta(timeBase, lastSumMisCH1, lastSumMisCH2, lastSumMisCH3);
    fileMaxNumber = numberOfMeas.text().toInt();
    fileNumber = 1;
}

void window::clickedStopErrTest()
{
    timerGraph->stop();
    timerWrite->stop();
    tcp.command_set(CMD_STOP_TEST, 0, 0);

    regRef.setDisabled(false);
    supplyGroup->setDisabled(false);
    currentGroup->setDisabled(false);
    writeData->setDisabled(false);
    sendCommand->setDisabled(false);
    stopErrTest.setDisabled(true);
    numberOfMeas.setDisabled(false);
    filenameLineEdit.setDisabled(false);
    filenamePath.setDisabled(false);
    RWCheckBox.setDisabled(false);
    ECCBox.setDisabled(false);
    VarTestBox((bool)RWCheckBox.checkState());

    if (!writeVarStat && !RWCheckBox.checkState())
        startErrTest.setDisabled(false);
    else
    {
        startErrTest.setDisabled(false);
    }

    graphs->setDisabled(true);
}

void window::clickedFilenamePath()
{
    QString filename = QFileDialog::getSaveFileName(this, tr("Select one or more files to open"), QDir::currentPath(), tr("Text Files (*.txt *.csv)"));
    filenameLineEdit.setText(filename);
    baseName = filename.left(filename.lastIndexOf("."));
    suffix = filename.mid(filename.lastIndexOf("."));
}

void window::SaveErrToTxt()
{
    QDateTime dateTime = QDateTime::currentDateTime();
    QString filename = QString("%1_%2_%3_%4_of_%5%6").arg(baseName).arg(dateTime.toString("dd-MM-yyyy_hh-mm")).arg("err").arg(fileNumber).arg(fileMaxNumber).arg(suffix);

    QFile file(filename);
    if (file.open(QIODevice::Append))
    {
        QTextStream stream(&file);
        stream << "####################\n";
        stream << "#Number of errors\n";
        stream << "#Test " << fileNumber << " of " << fileMaxNumber << "\n";
        stream << "#Date: " << dateTime.toString("dd.MM.yyyy") << " Time: " << dateTime.toString("hh:mm:ss") << "\n";
        stream << "#Number of checked Bytes: " << tcp.reg[NUMBER_OF_BITES] << ";\n";
        if (writeVarStat)
            stream << "#Pattern: Variable array;\n";
        else
        {
            stream << "#Pattern: " << setConst.text() << ";\n";
        }
        stream << "#R-W test:" << (bool)RWCheckBox.checkState() << "\n";
        stream << "#Test begin\n\n";
        stream << "CH;errors[b]\n";
        for (int i = 0; i < graph.xMisData.size(); i++)
            stream << graph.xMisData[i] << ";" << graph.yMisData[i] << "\n";

        stream << "\n#Test end \n";
        stream << "####################\n";
    }
    file.close();
    filename = QString("%1_%2_%3_%4_of_%5%6").arg(baseName).arg(dateTime.toString("dd-MM-yyyy_hh-mm")).arg("err").arg(fileNumber).arg(fileMaxNumber).arg(".pdf");
    graph.plotMis->savePdf(filename, 800, 480);
}

void window::SaveECCToTxt()
{
    QDateTime dateTime = QDateTime::currentDateTime();
    QString filename = QString("%1_%2_%3_%4_of_%5%6").arg(baseName).arg(dateTime.toString("dd-MM-yyyy_hh-mm")).arg("ECC").arg(fileNumber).arg(fileMaxNumber).arg(suffix);

    QFile file(filename);
    if (file.open(QIODevice::Append))
    {
        QTextStream stream(&file);
        stream << "####################\n";
        stream << "#Number of ECC ocurrance\n";
        stream << "#Test " << fileNumber << " of " << fileMaxNumber << "\n";
        stream << "#Date: " << dateTime.toString("dd.MM.yyyy") << " Time: " << dateTime.toString("hh:mm:ss") << "\n";
        stream << "#Number of checked Bytes: " << tcp.reg[NUMBER_OF_BITES] << ";\n";
        if (writeVarStat)
            stream << "#Pattern: Variable array;\n";
        else
        {
            stream << "#Pattern: " << setConst.text() << ";\n";
        }
        stream << "#R-W test:" << (bool)RWCheckBox.checkState() << "\n";
        stream << "#Test begin\n\n";
        stream << "CH;corrections[-]\n";
        for (int i = 0; i < graph.xMisDataECC.size(); i++)
            stream << graph.xMisDataECC[i] << ";" << graph.yMisDataECC[i] << "\n";

        stream << "\n#Test end \n";
        stream << "####################\n";
    }
    file.close();
    filename = QString("%1_%2_%3_%4_of_%5%6").arg(baseName).arg(dateTime.toString("dd-MM-yyyy_hh-mm")).arg("ECC").arg(fileNumber).arg(fileMaxNumber).arg(".pdf");
    graph.plotMisECC->savePdf(filename, 800, 480);
}
void window::SaveDeltaToTxt()
{
    QDateTime dateTime = QDateTime::currentDateTime();
    QString filename = QString("%1_%2_%3_%4_of_%5%6").arg(baseName).arg(dateTime.toString("dd-MM-yyyy_hh-mm")).arg("delta").arg(fileNumber).arg(fileMaxNumber).arg(suffix);
    QFile file(filename);
    if (file.open(QIODevice::Append))
    {
        QTextStream stream(&file);
        stream << "####################\n";
        stream << "#Delta of errors in time \n";
        stream << "#Test " << fileNumber << " of " << fileMaxNumber << "\n";
        stream << "#Date: " << dateTime.toString("dd.MM.yyyy") << " Time: " << dateTime.toString("hh:mm:ss") << "\n";
        stream << "#Number of checked Bytes: " << tcp.reg[NUMBER_OF_BITES] << ";\n";
        if (writeVarStat)
            stream << "#Pattern: Variable array;\n";
        else
        {

            stream << "#Pattern: " << setConst.text() << ";\n";
        }
        stream << "#R-W test:" << (bool)RWCheckBox.checkState() << "\n";
        stream << "#Test begin\n\n";
        stream << "Time [ms];delta CH1 [-];delta CH2 [-];delta CH3[-]\n";
        for (int i = 0; i < graph.xDeltaData.size(); i++)
            stream << graph.xDeltaData[i] << ";" << graph.yDeltaDataCH1[i] << ";" << graph.yDeltaDataCH2[i] << ";" << graph.yDeltaDataCH3[i] << "\n";

        stream << "\n#Test end \n";
        stream << "####################\n";
    }
    file.close();

    filename = QString("%1_%2_%3_%4_of_%5%6").arg(baseName).arg(dateTime.toString("dd-MM-yyyy_hh-mm")).arg("delta").arg(fileNumber).arg(fileMaxNumber).arg(".pdf");
    graph.plotDelta->savePdf(filename, 800, 480);
}
void window::SaveCurrToTxt()
{
    QDateTime dateTime = QDateTime::currentDateTime();
    QString filename = QString("%1_%2_%3_%4_of_%5%6").arg(baseName).arg(dateTime.toString("dd-MM-yyyy_hh-mm")).arg("curr").arg(fileNumber).arg(fileMaxNumber).arg(suffix);

    QFile file(filename);
    if (file.open(QIODevice::Append))
    {
        QTextStream stream(&file);
        stream << "####################\n";
        stream << "#Current\n";
        stream << "#Test " << fileNumber << " of " << fileMaxNumber << "\n";
        stream << "#Date: " << dateTime.toString("dd.MM.yyyy") << " Time: " << dateTime.toString("hh:mm:ss") << "\n";
        stream << "#Number of checked Bytes: " << tcp.reg[NUMBER_OF_BITES] << ";\n";
        if (writeVarStat)
            stream << "#Pattern: Variable array;\n";
        else
        {
            stream << "#Pattern: " << setConst.text().toUInt() << ";\n";
        }
        stream << "#Latch-up event treshold current CH1: " << tcp.reg[LATCH_UP_TRSH_CH1] << " mA;\n";
        stream << "#Latch-up event treshold current CH2: " << tcp.reg[LATCH_UP_TRSH_CH2] << " mA;\n";
        stream << "#Latch-up event treshold current CH3: " << tcp.reg[LATCH_UP_TRSH_CH3] << " mA;\n";
        stream << "#R-W test:" << (bool)RWCheckBox.checkState() << "\n";
        stream << "#Test begin\n\n";
        stream << "Time[ms];I_CH1 [mA];I_CH2 [mA];I_CH3 [mA];I_SELCH1 [mA];I_SELCH2 [mA];I_SELCH3 [mA]\n";
        for (int i = 0; i < graph.xCurrData.size(); i++)
            stream << graph.xCurrData[i] << ";" << graph.yCurrDataCH1[i] << ";" << graph.yCurrDataCH2[i] << ";" << graph.yCurrDataCH3[i] << ";" << graph.yLatchUpCH1[i] << ";" << graph.yLatchUpCH2[i] << ";" << graph.yLatchUpCH3[i] << "\n";
        stream << "\n#Test end \n";
        stream << "####################\n";
    }
    file.close();

    filename = QString("%1_%2_%3_%4_of_%5%6").arg(baseName).arg(dateTime.toString("dd-MM-yyyy_hh-mm")).arg("curr").arg(fileNumber).arg(fileMaxNumber).arg(".pdf");
    graph.plotCurr->savePdf(filename, 800, 480);
}

void window::stateChangedRW(bool)
{
    varTestCheckBox.setDisabled(!(bool)RWCheckBox.checkState());
    if (!(bool)RWCheckBox.checkState() == true)
    {
        varTestCheckBox.setChecked(false);
        startErrTest.setDisabled(false);
    }
    else
    {
        startErrTest.setDisabled(false);
    }
}
void window::VarTestBox(bool val)
{
    varTestCheckBox.setDisabled(!val);
}

void window::clickedVarTestCheckBox(bool)
{
    writeVarStat = (bool)varTestCheckBox.checkState();
}

void window::sliderChangedCH1()
{
    trshValueCH1.setText(QString::number(latchupSliderCH1->value()));
}
void window::sliderChangedCH2()
{
    trshValueCH2.setText(QString::number(latchupSliderCH2->value()));
}
void window::sliderChangedCH3()
{
    trshValueCH3.setText(QString::number(latchupSliderCH3->value()));
}
