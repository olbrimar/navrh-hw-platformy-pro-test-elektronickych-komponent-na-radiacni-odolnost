/*
 * general.h
 *
 *  Created on: 21 Dec 2022
 *      Author: MartinOl
 *
 *      Tento hlavickovy soubor definuje funkce, ktere je mozne pouzit v jakemkoliv hlavickovem souboru
 *      printf, spi read, spi write, chipselect, remapADC, setSuply
 */

#ifndef INC_GENERAL_H_
#define INC_GENERAL_H_
#include<stdio.h>

//functions
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#define TEMP_VAL 0x00
#define CURR_CH1_VAL 0x01
#define CURR_CH2_VAL 0x02
#define CURR_CH3_VAL 0x03
#define SUPPLY_CH1 0x04
#define SUPPLY_CH2 0x05
#define SUPPLY_CH3 0x06
#define WRITE_STAT 0x07
#define TEST_STAT 0x08
#define MEAS_CH1_RES 0x9
#define MEAS_CH2_RES 0xA
#define MEAS_CH3_RES 0xB
#define NUMBER_OF_BITES 0xC
#define LATCH_UP_TRSH_CH1 0xD
#define LATCH_UP_TRSH_CH2 0xE
#define LATCH_UP_TRSH_CH3 0xF
#define LATCH_UP_EVENT_CH1 0x10
#define LATCH_UP_EVENT_CH2 0x11
#define LATCH_UP_EVENT_CH3 0x12

#define CMD_SET_SUPPLY 0x80
#define CMD_WRITE_CONST 0x81
#define CMD_WRITE_VAR 0x82
#define CMD_START_TEST 0x83
#define CMD_STOP_TEST 0x84
#define CMD_NUMBER_OF_BYTES 0x85
#define CMD_LATCHUP_LEVEL 0x86
float RemapADC(uint32_t value, double retuMin, double retuMax);
void ModuleSetSupply(int channel, int stav);
void ProcessADC();
void ToggleLed();
void ModuleResetSupply(int channel);


#endif /* INC_GENERAL_H_ */
