#include <QWidget>
#include <QTcpSocket>

class TCP : public QWidget
{
    Q_OBJECT

    QByteArray data;

public:
    TCP(QWidget *parent = 0);
    uint32_t reg[22];
    void connection(QString hostname, quint16 port);
    QTcpSocket *socket;
    uint32_t command_get(uint32_t addr);
    void command_set(uint32_t cmd, uint32_t channel, uint32_t data = 0);
    void loadReg();
    void loadCurr();

public slots:
    void connected();
    void disconnected();
    void readyRead();
};
