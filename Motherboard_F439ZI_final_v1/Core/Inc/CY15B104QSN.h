/*
 * CY15B104QSN.h
 *
 *  Created on: Dec 21, 2022
 *      Author: MartinOl
 */

#ifndef INC_CY15B104QSN_H_
#define INC_CY15B104QSN_H_
//Maximalni velikost pameti/pocet zasahnutych bunek
#define MAX_SPI_BUF 256

void DeviceInit();
void DeviceWriteData(int channel, uint8_t *data, uint32_t addr, int lenght);
void DeviceReadData(int channel, uint32_t addr, int lenght);
void DeviceCreateConstantData(uint8_t value);
void DeviceCreateVariableData();
void DeviceStartWrite();
void DeviceStartRead();
void DeviceWriteDone();
void DeviceReadDone();
void DeviceChange_NUMBER_READ_TEST(uint32_t value);
uint32_t DeviceValidate(uint8_t *dataIN, int len);
uint32_t CY15B104QSN_ReadECCRegister(int channel);
void CY15B104QSN_ClearECCRegister(int channel);
#endif /* INC_CY15B104QSN_H_ */
