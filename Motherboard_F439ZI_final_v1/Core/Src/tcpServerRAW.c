/*
  ***************************************************************************************************************
  ***************************************************************************************************************
  ***************************************************************************************************************

  File:		  	   tcpServerRAW.c
  Modified By:     ControllersTech.com
  Updated:    	   26-Jul-2021

  ***************************************************************************************************************
  Copyright (C) 2017 ControllersTech.com

  This is a free software under the GNU license, you can redistribute it and/or modify it under the terms
  of the GNU General Public License version 3 as published by the Free Software Foundation.
  This software library is shared with public for educational purposes, without WARRANTY and Author is not liable for any damages caused directly
  or indirectly by this software, read more about this on the GNU General Public License.

  ***************************************************************************************************************
*/


/**
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of and a contribution to the lwIP TCP/IP stack.
 *
 * Credits go to Adam Dunkels (and the current maintainers) of this software.
 *
 * Christiaan Simons rewrote this file to get a more stable  application.
 *
 **/

 /* This file was modified by ST */

#include "tcpserverRAW.h"

#include "lwip/tcp.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "general.h"
#include "CY15B104QSN.h"


/*  protocol states */
enum tcp_server_states
{
  ES_NONE = 0,
  ES_ACCEPTED,
  ES_RECEIVED,
  ES_CLOSING
};

/* structure for maintaining connection infos to be passed as argument
   to LwIP callbacks*/
struct tcp_server_struct
{
  u8_t state;             /* current connection state */
  u8_t retries;
  struct tcp_pcb *pcb;    /* pointer on the current tcp_pcb */
  struct pbuf *p;         /* pointer on the received/to be transmitted pbuf */
};



static err_t tcp_server_accept(void *arg, struct tcp_pcb *newpcb, err_t err);
static err_t tcp_server_recv(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err);
static void tcp_server_error(void *arg, err_t err);
static err_t tcp_server_poll(void *arg, struct tcp_pcb *tpcb);
static err_t tcp_server_sent(void *arg, struct tcp_pcb *tpcb, u16_t len);
static void tcp_server_send(struct tcp_pcb *tpcb, struct tcp_server_struct *es);
static void tcp_server_connection_close(struct tcp_pcb *tpcb, struct tcp_server_struct *es);
static void tcp_server_acknowledge(struct tcp_pcb *tpcb, void *arg);
static void tcp_server_handle (struct tcp_pcb *tpcb, struct tcp_server_struct *es);

extern UART_HandleTypeDef huart3;
extern uint32_t reg[22];

#define minAdr 	0x00
#define maxAdr 	0x15
#define minCom	0x80
#define maxCom	0x86

#define TEMP_VAL 0x00
#define CURR_CH1_VAL 0x01
#define CURR_CH2_VAL 0x02
#define CURR_CH3_VAL 0x03
#define SUPPLY_CH1 0x04
#define SUPPLY_CH2 0x05
#define SUPPLY_CH3 0x06
#define WRITE_STAT 0x07
#define TEST_STAT 0x08
#define MEAS_CH1_RES 0x9
#define MEAS_CH2_RES 0xA
#define MEAS_CH3_RES 0xB
#define NUMBER_OF_BITES 0xC
#define LATCH_UP_TRSH_CH1 0xD
#define LATCH_UP_TRSH_CH2 0xE
#define LATCH_UP_TRSH_CH3 0xF
#define LATCH_UP_EVENT_CH1 0x10
#define LATCH_UP_EVENT_CH2 0x11
#define LATCH_UP_EVENT_CH3 0x12
#define ECC_CORRECTION_CH1 0x13
#define ECC_CORRECTION_CH2 0x14
#define ECC_CORRECTION_CH3 0x15

#define CMD_SET_SUPPLY 0x80
#define CMD_WRITE_CONST 0x81
#define CMD_WRITE_VAR 0x82
#define CMD_START_TEST 0x83
#define CMD_STOP_TEST 0x84
#define CMD_NUMBER_OF_BYTES 0x85
#define CMD_LATCHUP_LEVEL 0x86



/* Impementation for the TCP Server
   1. Create TCP Block.
   2. Bind the Block to server address, and port.
   3. Listen for the  incoming requests by the client
   4. Accept the Request, and now the server is ready for the data transfer
 */

void tcp_server_init(void)
{

	/* 1. create new tcp pcb */
	struct tcp_pcb *tpcb;

	tpcb = tcp_new();

	err_t err;

	/* 2. bind _pcb to port 7 ( protocol) */
	ip_addr_t myIPADDR;
	IP_ADDR4(&myIPADDR, 192, 168, 0, 111);
	err = tcp_bind(tpcb, &myIPADDR, 7);

	if (err == ERR_OK)
	{
		/* 3. start tcp listening for _pcb */
		tpcb = tcp_listen(tpcb);

		/* 4. initialize LwIP tcp_accept callback function */
		tcp_accept(tpcb, tcp_server_accept);
	}
	else
	{
		/* deallocate the pcb */
		memp_free(MEMP_TCP_PCB, tpcb);
	}
}

/**
  * @brief  This function is the implementation of tcp_accept LwIP callback
  * @param  arg: not used
  * @param  newpcb: pointer on tcp_pcb struct for the newly created tcp connection
  * @param  err: not used
  * @retval err_t: error status
  */
static err_t tcp_server_accept(void *arg, struct tcp_pcb *newpcb, err_t err)
{
  err_t ret_err;
  struct tcp_server_struct *es;

  LWIP_UNUSED_ARG(arg);
  LWIP_UNUSED_ARG(err);

  /* set priority for the newly accepted tcp connection newpcb */
  tcp_setprio(newpcb, TCP_PRIO_MIN);

  /* allocate structure es to maintain tcp connection information */
  es = (struct tcp_server_struct *)mem_malloc(sizeof(struct tcp_server_struct));
  if (es != NULL)
  {
    es->state = ES_ACCEPTED;
    es->pcb = newpcb;
    es->retries = 0;
    es->p = NULL;

    /* pass newly allocated es structure as argument to newpcb */
    tcp_arg(newpcb, es);

    /* initialize lwip tcp_recv callback function for newpcb  */
    tcp_recv(newpcb, tcp_server_recv);

    /* initialize lwip tcp_err callback function for newpcb  */
    tcp_err(newpcb, tcp_server_error);

    /* initialize lwip tcp_poll callback function for newpcb */
    tcp_poll(newpcb, tcp_server_poll, 0);

    ret_err = ERR_OK;
  }
  else
  {
    /*  close tcp connection */
    tcp_server_connection_close(newpcb, es);
    /* return memory error */
    ret_err = ERR_MEM;
  }
  return ret_err;
}


/**
  * @brief  This function is the implementation for tcp_recv LwIP callback
  * @param  arg: pointer on a argument for the tcp_pcb connection v nasem pripade es??
  * @param  tpcb: pointer on the tcp_pcb connection
  * @param  pbuf: pointer on the received pbuf, pbuf je struktura obsahujici ukazatel na prichazejici data v polozce payload
  * @param  err: error information regarding the reveived pbuf (nwm proc)
  * @retval err_t: error code
  */
static err_t tcp_server_recv(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err) //Tato funkce je volana, kdyz prijdou data a urcuje co se s nimi ma dit
{
  struct tcp_server_struct *es;
  err_t ret_err;

  LWIP_ASSERT("arg != NULL",arg != NULL);

  es = (struct tcp_server_struct *)arg;

  /* if we receive an empty tcp frame from client => close connection */
  if (p == NULL)
  {
    /* remote host closed connection */
    es->state = ES_CLOSING;
    if(es->p == NULL)
    {
       /* we're done sending, close connection */
       tcp_server_connection_close(tpcb, es);
    }
    else
    {
      /* we're not done yet */
      /* acknowledge received packet */
      tcp_sent(tpcb, tcp_server_sent);

      /* send remaining data*/
      tcp_server_send(tpcb, es);
    }
    ret_err = ERR_OK;
  }
  /* else : a non empty frame was received from client but for some reason err != ERR_OK */
  else if(err != ERR_OK) //pokud
  {
    /* free received pbuf*/
    if (p != NULL)
    {
      es->p = NULL;
      pbuf_free(p);
    }
    ret_err = err;
  }
  else if(es->state == ES_ACCEPTED)
  {
    /* first data chunk in p->payload */
    es->state = ES_RECEIVED;

    /* store reference to incoming pbuf (chain) */
    es->p = p;

    /* initialize LwIP tcp_sent callback function */
    tcp_sent(tpcb, tcp_server_sent);

    /* handle the received data */
    tcp_server_handle(tpcb, es);

    ret_err = ERR_OK;
  }
  else if (es->state == ES_RECEIVED)
  {
    /* more data received from client and previous data has been already sent*/
    if(es->p == NULL)
    {
      es->p = p;

      /* handle the received data */
      tcp_server_handle(tpcb, es);
    }
    else
    {
      struct pbuf *ptr;

      /* chain pbufs to the end of what we recv'ed previously  */
      ptr = es->p;
      pbuf_chain(ptr,p);
    }
    ret_err = ERR_OK;
  }
  else if(es->state == ES_CLOSING)
  {
    /* odd case, remote side closing twice, trash data */
    tcp_recved(tpcb, p->tot_len);
    es->p = NULL;
    pbuf_free(p);
    ret_err = ERR_OK;
  }
  else
  {
    /* unknown es->state, trash data  */
    tcp_recved(tpcb, p->tot_len);
    es->p = NULL;
    pbuf_free(p);
    ret_err = ERR_OK;
  }
  return ret_err;
}

/**
  * @brief  This function implements the tcp_err callback function (called
  *         when a fatal tcp_connection error occurs.
  * @param  arg: pointer on argument parameter
  * @param  err: not used
  * @retval None
  */
static void tcp_server_error(void *arg, err_t err)
{
  struct tcp_server_struct *es;

  LWIP_UNUSED_ARG(err);

  es = (struct tcp_server_struct *)arg;
  if (es != NULL)
  {
    /*  free es structure */
    mem_free(es);
  }
}

static void tcp_server_acknowledge(struct tcp_pcb *tpcb, void *arg){
	struct tcp_server_struct *es;
	es = (struct tcp_server_struct *)arg;
	struct pbuf *ptr;
	ptr = es->p;

	u16_t plen;
	u8_t freed;

	      plen = ptr->len;

	      /* continue with next pbuf in chain (if any) */
	      es->p = ptr->next;

	      if(es->p != NULL)
	      {
	        /* increment reference count for es->p */
	        pbuf_ref(es->p);
	      }

	     /* chop first pbuf from chain */
	      do
	      {
	        /* try hard to free pbuf */
	        freed = pbuf_free(ptr);
	      }
	      while(freed == 0);
	     /* we can read more data now */
	     tcp_recved(tpcb, plen);


}

/**
  * @brief  This function implements the tcp_poll LwIP callback function
  * @param  arg: pointer on argument passed to callback
  * @param  tpcb: pointer on the tcp_pcb for the current tcp connection
  * @retval err_t: error code
  */
static err_t tcp_server_poll(void *arg, struct tcp_pcb *tpcb)
{
  err_t ret_err;
  struct tcp_server_struct *es;

  es = (struct tcp_server_struct *)arg;
  if (es != NULL)
  {
    if (es->p != NULL)
    {
      tcp_sent(tpcb, tcp_server_sent);
      /* there is a remaining pbuf (chain) , try to send data */
      tcp_server_send(tpcb, es);

    }
    else
    {
      /* no remaining pbuf (chain)  */
      if(es->state == ES_CLOSING)
      {
        /*  close tcp connection */
        tcp_server_connection_close(tpcb, es);
      }
    }
    ret_err = ERR_OK;
  }
  else
  {
    /* nothing to be done */
    tcp_abort(tpcb);
    ret_err = ERR_ABRT;
  }
  return ret_err;
}

/**
  * @brief  This function implements the tcp_sent LwIP callback (called when ACK
  *         is received from remote host for sent data)
  * @param  None
  * @retval None
  */
static err_t tcp_server_sent(void *arg, struct tcp_pcb *tpcb, u16_t len) //funkce volana pro potvrzeni prijeti
{
  struct tcp_server_struct *es;

  LWIP_UNUSED_ARG(len);

  es = (struct tcp_server_struct *)arg;
  es->retries = 0;

  if(es->p != NULL)
  {
    /* still got pbufs to send */
    tcp_sent(tpcb, tcp_server_sent);
    tcp_server_send(tpcb, es);

  }
  else
  {
    /* if no more data to send and client closed connection*/
    if(es->state == ES_CLOSING)
      tcp_server_connection_close(tpcb, es);
  }

  return ERR_OK;
}


/**
  * @brief  This function is used to send data for tcp connection
  * @param  tpcb: pointer on the tcp_pcb connection
  * @param  es: pointer on _state structure
  * @retval None
  */
static void tcp_server_send(struct tcp_pcb *tpcb, struct tcp_server_struct *es)
{
  struct pbuf *ptr;
  err_t wr_err = ERR_OK;

  while ((wr_err == ERR_OK) && (es->p != NULL) && (es->p->len <= tcp_sndbuf(tpcb)))
  {
    /* get pointer on pbuf from es structure */
    ptr = es->p;

    /* enqueue data for transmission */
    wr_err = tcp_write(tpcb, ptr->payload, ptr->len, 1);

    if (wr_err == ERR_OK)
    {

      tcp_server_acknowledge(tpcb, es);

   }
   else if(wr_err == ERR_MEM)
   {
      /* we are low on memory, try later / harder, defer to poll */
     es->p = ptr;
   }
   else
   {
     /* other problem ?? */
   }
  }
}

/**
  * @brief  This functions closes the tcp connection
  * @param  tcp_pcb: pointer on the tcp connection
  * @param  es: pointer on _state structure
  * @retval None
  */
static void tcp_server_connection_close(struct tcp_pcb *tpcb, struct tcp_server_struct *es)
{

  /* remove all callbacks */
  tcp_arg(tpcb, NULL);
  tcp_sent(tpcb, NULL);
  tcp_recv(tpcb, NULL);
  tcp_err(tpcb, NULL);
  tcp_poll(tpcb, NULL, 0);

  /* delete es structure */
  if (es != NULL)
  {
    mem_free(es);
  }

  /* close tcp connection */
  tcp_close(tpcb);
}

/* Handle the incoming TCP Data */



static void tcp_server_handle (struct tcp_pcb *tpcb, struct tcp_server_struct *es)
{

	struct tcp_server_struct *esTx;
	int adrCom = 0;
	int channel = 0;
	int data = 0;
	esTx = es;
	char buf[100];
	char *partData;
	int counter = 0;

	memset (buf, '\0', 100);
	strncpy(buf, (char *)es->p->payload, es->p->tot_len);
	partData = strtok(buf, "|");

		while(partData != NULL){

			switch (counter) {
				case 0:
					adrCom = strtol(partData, NULL, 16);
					break;
				case 1:
					channel = strtol(partData, NULL, 10);
					break;
				case 2:
					data = strtol(partData, NULL, 16);
					break;
			}
			counter++;
			partData = strtok(NULL, "|");
		}

		/*Pokud prijde adresa registru (paket GET)*/
		if ((adrCom >= minAdr) && (adrCom <= maxAdr)){
			memset(buf, '\0', 100);
			sprintf(buf, "0x%X|%lu|", adrCom, reg[adrCom]);



			esTx->p->payload = (void *)buf;
			esTx->p->tot_len = strlen (buf);
			esTx->p->len = strlen (buf);
			tcp_server_send(tpcb, esTx);

			if(adrCom >= LATCH_UP_EVENT_CH1)
				reg[adrCom] = 0;

		}

		/*Pokud prijde prikaz*/
		else if ((adrCom >= minCom) && (adrCom <= maxCom)) {
			switch (adrCom) {

				/*Nastaveni stavu napajeni*/
				case 0x80:
					ModuleSetSupply(channel, data);
					break;

				/*Pusteni zapisu konstant*/
				case 0x81:
					reg[WRITE_STAT] = 1;
					DeviceCreateConstantData((uint8_t) data);
					DeviceStartWrite();
					break;

				/*Pusteni zapisu promenneho pole*/
				case 0x82:
					reg[WRITE_STAT] = 1;
					DeviceCreateVariableData();
					DeviceStartWrite();
					break;

				/*Spusteni testu*/
				case 0x83:
					reg[TEST_STAT] = 1;
					DeviceStartRead();
					break;

				/*Vypnuti testu*/
				case 0x84:
					reg[TEST_STAT] = 0;
					reg[WRITE_STAT] = 0;
					break;

				/*Nastaveni rozsahu testu*/
				case 0x85:
					reg[NUMBER_OF_BITES] =  (uint32_t)data;
					DeviceChange_NUMBER_READ_TEST((uint32_t)data);
					break;

				/*Nastaveni latch-up level*/
				case 0x86:
					printf("Channel: %d, Data: %d\r\n", channel, data);
					reg[0xC + channel] = (uint32_t)data;
					break;

			}

			tcp_server_acknowledge(tpcb, es);
		}
		else {
			tcp_server_acknowledge(tpcb, es);
		}









}
