#include <Qwidget>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include "TCP.h"
#include <QCheckBox>
#include <QPixmap>
#include <QTimer>
#include "graph.h"
#include <QSlider>
#include <QDateTime>
#include <QGroupBox>

class window : public QWidget
{
    Q_OBJECT
    QHBoxLayout hbox, hbox_supply_CH1, hbox_supply_CH2, hbox_supply_CH3, hbox_curr_CH1, hbox_curr_CH2, hbox_curr_CH3, hbox_temp, hbox_sendCommand, hbox_writeConstData, hbox_writeVarData, hbox_varTest, hbox_saveBut, hbox_trshCH1, hbox_trshCH2, hbox_trshCH3, hbox_startTestNumberOfMeas, hbox_startTestPath, hbox_startTestCheckBox, hbox_errBar;
    QVBoxLayout vbox_left, vbox_right, vbox_tcp, vbox_supply, vbox_curr, vbox_graph, vbox_writeData, vbox_startTest;

    QLabel CH1currLabel, CH1curr, CH2curr, CH2currLabel, CH3curr, CH3currLabel, currUnitCH1, currUnitCH2, currUnitCH3, tempLabel, temp, tempUnit, writeDataLabel, pictureLabel, trshLabelCH1, trshLabelCH2, trshLabelCH3, trshUnitCH1, trshUnitCH2, trshUnitCH3, trshValueCH1, trshValueCH2, trshValueCH3, numberOfMeasLabel;
    QLineEdit sendCommandLine, setConst, constTestChannel, numberOfMeas, filenameLineEdit;
    QPushButton TCPcondis, regRef, sendCommandBut, constValWrite, varValWrite, startErrTest, stopErrTest, currSave, misSave, deltaSave, filenamePath;
    QCheckBox CH1, CH2, CH3, RWCheckBox, varTestCheckBox, ECCBox;
    QPixmap picture;
    TCP tcp;
    QTimer *timerGraph, *timerWrite, *timerWR;
    Graph graph;
    QSlider *latchupSliderCH1, *latchupSliderCH2, *latchupSliderCH3;
    QGroupBox *tcpGroup, *supplyGroup, *tempGroup, *currentGroup, *sendCommand, *writeData, *startTest, *graphs;

    QString baseName, suffix;

    uint32_t fileNumber = 0;
    uint32_t fileMaxNumber = 0;
    uint32_t timerTime = 1000;
    volatile uint32_t writeCheckInterval = 1000;
    uint32_t numberOfMeasBytesSave = 0;
    bool writeVarStat = false;

public:
    window(QWidget *parent = 0);
    void SaveErrToTxt();
    void SaveDeltaToTxt();
    void SaveCurrToTxt();
    void SaveECCToTxt();
    void VarTestBox(bool val);

public slots:
    void clickedConDis();
    void clickedRef();
    void changePushText();
    void stateChangedCH1(bool);
    void stateChangedCH2(bool);
    void stateChangedCH3(bool);
    void stateChangedRW(bool);
    void clickedSend();
    void clickedConstValWrite();
    void timerGraphTimeout();
    void clickedVarWrite();
    void clickedStartErrTest();
    void clickedStopErrTest();
    void timerWriteTimeout();
    void clickedFilenamePath();
    void clickedVarTestCheckBox(bool);
    void sliderChangedCH1();
    void sliderChangedCH2();
    void sliderChangedCH3();
};