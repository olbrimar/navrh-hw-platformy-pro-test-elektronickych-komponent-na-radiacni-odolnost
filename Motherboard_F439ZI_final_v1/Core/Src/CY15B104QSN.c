/*
 * CY15B104QSN.c
 *
 *  Created on: Jan 25, 2023
 *      Author: MartinOl
 */

#include<stdio.h>
#include<string.h>
#include "CY15B104QSN.h"
#include "SPI.h"
#include "main.h"
//#include "config.h"

#define TEMP_VAL 0x00
#define CURR_CH1_VAL 0x01
#define CURR_CH2_VAL 0x02
#define CURR_CH3_VAL 0x03
#define SUPPLY_CH1 0x04
#define SUPPLY_CH2 0x05
#define SUPPLY_CH3 0x06
#define WRITE_STAT 0x07
#define TEST_STAT 0x08
#define MEAS_CH1_RES 0x9
#define MEAS_CH2_RES 0xA
#define MEAS_CH3_RES 0xB
#define NUMBER_OF_BITES 0xC
#define LATCH_UP_TRSH_CH1 0xD
#define LATCH_UP_TRSH_CH2 0xE
#define LATCH_UP_TRSH_CH3 0xF
#define LATCH_UP_EVENT_CH1 0x10
#define LATCH_UP_EVENT_CH2 0x11
#define LATCH_UP_EVENT_CH3 0x12
#define ECC_CORRECTION_CH1 0x13
#define ECC_CORRECTION_CH2 0x14
#define ECC_CORRECTION_CH3 0x15

#define CMD_SET_SUPPLY 0x80
#define CMD_WRITE_CONST 0x81
#define CMD_WRITE_VAR 0x82
#define CMD_START_TEST 0x83
#define CMD_STOP_TEST 0x84
#define CMD_NUMBER_OF_BYTES 0x85
#define CMD_LATCHUP_LEVEL 0x86

extern SPI_HandleTypeDef hspi1;
extern uint32_t reg[22];

static const uint8_t READ_INST = 0b00000011;
static const uint8_t WRITE_INST = 0b00000010;
static const uint8_t WRDI_INST = 0b00000100;
static const uint8_t WREN_INST = 0b00000110;
static const uint8_t RDSR_INST = 0b00000101;
static const uint8_t WRSR_INST = 0b00000001;
static const uint8_t RDID_INST = 0b10011111;
static const uint8_t RUID_INST = 0b01001100;
static const uint8_t RDAR_INST = 0b01100101;
static const uint8_t CECCR_INST = 0x1B;

static const uint32_t ECC_COUNTER_REG_ADDRES = 0x00008A;


static uint8_t packet[MAX_SPI_BUF + 4];
static uint8_t receiveData[MAX_SPI_BUF + 4];
static uint8_t dataOUT[MAX_SPI_BUF];
static uint8_t activeChannel = 0;
static uint32_t addres = 0;
static uint32_t NUMBER_OF_TESTED_BYTES = 0x80000; //Pocet kontrolovanych B (musi byt nasobkem 8) max. 0x7FFFF
uint32_t NUMBER_OF_WROTE_READ_BYTE = 0;

void DeviceInit() {
	SetChipSelect(0xFF);
	reg[NUMBER_OF_BITES] = NUMBER_OF_TESTED_BYTES;
}

void DeviceCreateConstantData(uint8_t value) {
	memset(dataOUT, value, sizeof(dataOUT));
}

void DeviceCreateVariableData() {
	for (int i = 0; i < sizeof(dataOUT); i++) {
		dataOUT[i] = i;
	}
}

void DeviceWriteData(int channel, uint8_t *data, uint32_t addr, int lenght) {

	memset(packet, 0, sizeof(packet));
	packet[0] = WRITE_INST;
	packet[1] = (addr >> 16) & 0xFF;
	packet[2] = (addr >> 8) & 0xFF;
	packet[3] = (addr) & 0xFF;

	memcpy(&packet[4], data, lenght);

	SetChipSelect(channel);
	HAL_SPI_Transmit(&hspi1, (uint8_t*) &WREN_INST, 1, 10);
	SetChipSelect(0xFF);

	SetChipSelect(channel);
	HAL_SPI_Transmit_DMA(&hspi1, (uint8_t*) packet, lenght + 4);

}
void DeviceReadData(int channel, uint32_t addr, int lenght) {

	memset(packet, 0, sizeof(packet));
	memset(receiveData, 0, sizeof(receiveData));

	packet[0] = READ_INST;
	packet[1] = (addr >> 16) & 0xFF;
	packet[2] = (addr >> 8) & 0xFF;
	packet[3] = (addr) & 0xFF;

	SetChipSelect(channel);
	HAL_SPI_Transmit(&hspi1, (uint8_t*) &WREN_INST, 1, 10);
	SetChipSelect(0xFF);

	SetChipSelect(channel);
	HAL_SPI_TransmitReceive_DMA(&hspi1, (uint8_t*) packet,
			(uint8_t*) receiveData, lenght + 4);

	addres = addres + lenght;
}

void DeviceStartWrite() {
	activeChannel = 1;
	DeviceWriteData(activeChannel, dataOUT, addres, MAX_SPI_BUF);
	addres += MAX_SPI_BUF;
}

void DeviceWriteDone() {

	if ((addres == NUMBER_OF_TESTED_BYTES) && (activeChannel < 3)) {
		activeChannel++;
		addres = 0;
		DeviceWriteData(activeChannel, dataOUT, addres, MAX_SPI_BUF);
		addres += MAX_SPI_BUF;
	} else if (addres == NUMBER_OF_TESTED_BYTES || reg[WRITE_STAT] == 0) {
		reg[WRITE_STAT] = 0;
		addres = 0;
	} else {
		DeviceWriteData(activeChannel, dataOUT, addres, MAX_SPI_BUF);
		addres += MAX_SPI_BUF;
	}
}

void DeviceStartRead() {
	addres = 0;
	activeChannel = 1;
	DeviceReadData(activeChannel, addres, MAX_SPI_BUF);
	reg[MEAS_CH1_RES] = 0;
	reg[MEAS_CH2_RES] = 0;
	reg[MEAS_CH3_RES] = 0;
}

void DeviceReadDone() {

	reg[0x08 + activeChannel] = reg[0x08 + activeChannel]+ DeviceValidate(&receiveData[4], MAX_SPI_BUF);
	reg[0x12 + activeChannel] = reg[0x12 + activeChannel] + CY15B104QSN_ReadECCRegister(activeChannel);

	if ((addres == NUMBER_OF_TESTED_BYTES) && (activeChannel < 3)) {
		addres = 0;
		activeChannel++;
		DeviceReadData(activeChannel, addres, MAX_SPI_BUF);
	} else if ((addres == NUMBER_OF_TESTED_BYTES) || reg[TEST_STAT] == 0) {
		reg[TEST_STAT] = 0;
		addres = 0;
		activeChannel = 0;
	} else {
		DeviceReadData(activeChannel, addres, MAX_SPI_BUF);
	}

}

void DeviceChange_NUMBER_READ_TEST(uint32_t value) {
	NUMBER_OF_TESTED_BYTES = value;
}

uint32_t DeviceValidate(uint8_t *dataIN, int len) {
	uint32_t numberOfmis = 0;
	if (memcmp(dataIN, dataOUT, MAX_SPI_BUF) != 0) {

		for (int i = 0; i < MAX_SPI_BUF; i++) {
			for (int j = 0; j < 8; j++) {
				numberOfmis = numberOfmis
						+ (((dataIN[i] >> j) & (0b00000001))
								^ ((dataOUT[i] >> j) & (0b00000001)));
			}
		}
		return numberOfmis;
	} else {
		return 0;
	}

}

uint32_t CY15B104QSN_ReadECCRegister(int channel){
	uint8_t pckt[5];
	uint8_t pckt_rcv[5];
	memset(pckt, 0, sizeof(pckt));
	memset(pckt_rcv, 0, sizeof(pckt_rcv));
	pckt[0] = RDAR_INST;
	pckt[1] = (ECC_COUNTER_REG_ADDRES >> 16) & 0xFF;
	pckt[2] = (ECC_COUNTER_REG_ADDRES >> 8) & 0xFF;
	pckt[3] = (ECC_COUNTER_REG_ADDRES) & 0xFF;

	SetChipSelect(channel);
	HAL_SPI_TransmitReceive(&hspi1, (uint8_t *)pckt, (uint8_t *)pckt_rcv, sizeof(pckt), 10);
	SetChipSelect(0xFF);

	CY15B104QSN_ClearECCRegister(channel);

	return pckt_rcv[4];

}

void CY15B104QSN_ClearECCRegister(int channel){
		SetChipSelect(channel);
		HAL_SPI_Transmit(&hspi1, (uint8_t *)&WREN_INST, 1, 10);
		SetChipSelect(0xFF);
		SetChipSelect(channel);
		HAL_SPI_Transmit(&hspi1, (uint8_t *)&CECCR_INST, 1, 10);
		SetChipSelect(0xFF);
}

