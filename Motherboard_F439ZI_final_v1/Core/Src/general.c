/*
 * general.c
 *
 *  Created on: 21 Dec 2022
 *      Author: MartinOl
 */

#include "general.h"
#include "main.h"
#include<stdio.h>
#include<string.h>
#include"CY15B104QSN.h"

extern UART_HandleTypeDef huart3;
extern uint32_t reg[22];
extern uint32_t dataADCCH1[2];
extern uint32_t dataADCCH3;
extern uint8_t adc1result;
extern uint8_t adc3result;
float proud[3];
float currCH1[32];
float currCH2[32];
float currCH3[32];

float sumCurrCH1 = 0;
float sumCurrCH2 = 0;
float sumCurrCH3 = 0;

uint8_t currPointerCH1 = 0;
uint8_t currPointerCH2 = 0;
uint8_t currPointerCH3 = 0;

void ToggleLed(){

	HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_8);

}


PUTCHAR_PROTOTYPE
{
  /* Place your implementation of fputc here */
  /* e.g. write a character to the USART1 and Loop until the end of transmission */
  HAL_UART_Transmit(&huart3, (uint8_t *)&ch, 1, 0xFFFF);

  return ch;
}

float RemapADC(uint32_t value, double retuMin, double retuMax){

	return value*(retuMax - retuMin)/4096;

}

void ModuleSetSupply(int channel, int stav){

		switch (channel) {
			case 1:
				HAL_GPIO_WritePin(GPIOE, GPIO_PIN_15, stav ? GPIO_PIN_SET : GPIO_PIN_RESET);
				reg[SUPPLY_CH1] = stav;
				if(stav == 1)
					DeviceInit(1);
				break;

			case 2:
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, stav ? GPIO_PIN_SET : GPIO_PIN_RESET);
				reg[SUPPLY_CH2] = stav;
				if(stav == 1)
					DeviceInit(2);
				break;

			case 3:
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_11, stav ? GPIO_PIN_SET : GPIO_PIN_RESET);
				reg[SUPPLY_CH3] = stav;
				if(stav == 1)
					DeviceInit(3);
				break;

		}


}

void ProcessADC(){

	if(adc1result == 1){
		adc1result = 0;
		if((currPointerCH1 == ((sizeof(currCH1)/sizeof(float)) - 1)) && (currPointerCH2 == ((sizeof(currCH2)/sizeof(float)) - 1))){
			currCH1[currPointerCH1] = RemapADC(dataADCCH1[1], 0, 500);
			currCH2[currPointerCH2] = RemapADC(dataADCCH1[0], 0, 500);

			if(currCH1[currPointerCH1] >= reg[LATCH_UP_TRSH_CH1]){
				ModuleResetSupply(1);
				reg[TEST_STAT] = 0;
				reg[WRITE_STAT] = 0;
				memcpy( &reg[LATCH_UP_EVENT_CH1], &currCH1[currPointerCH1], sizeof(uint32_t));

				}

			if(currCH2[currPointerCH2] >= reg[LATCH_UP_TRSH_CH2]){
				ModuleResetSupply(2);
				reg[TEST_STAT] = 0;
				reg[WRITE_STAT] = 0;
				memcpy( &reg[LATCH_UP_EVENT_CH2], &currCH2[currPointerCH2], sizeof(uint32_t));
				}

			for(int i = 0; i < (sizeof(currCH1)/sizeof(currCH1[0])); i++){
				sumCurrCH1 += currCH1[i];
				sumCurrCH2 += currCH2[i];
			}



			sumCurrCH1 = sumCurrCH1/(sizeof(currCH1)/sizeof(currCH1[0]));
			sumCurrCH2 = sumCurrCH2/(sizeof(currCH2)/sizeof(currCH2[0]));

			currPointerCH1 = 0;
			currPointerCH2 = 0;
			memcpy( &reg[CURR_CH1_VAL], &sumCurrCH1, sizeof(uint32_t));
			memcpy( &reg[CURR_CH2_VAL], &sumCurrCH2, sizeof(uint32_t));
		}
		else{
			currCH1[currPointerCH1] = RemapADC(dataADCCH1[1], 0, 500);
			currCH2[currPointerCH2] = RemapADC(dataADCCH1[0], 0, 500);

			if(currCH1[currPointerCH1] >= reg[LATCH_UP_TRSH_CH1]){
				 ModuleResetSupply(1);
				reg[TEST_STAT] = 0;
				reg[WRITE_STAT] = 0;
				memcpy( &reg[LATCH_UP_EVENT_CH1], &currCH1[currPointerCH1], sizeof(uint32_t));
						}
			if(currCH2[currPointerCH2] >= reg[LATCH_UP_TRSH_CH2]){
				 ModuleResetSupply(2);
				reg[TEST_STAT] = 0;
				reg[WRITE_STAT] = 0;
				memcpy( &reg[LATCH_UP_EVENT_CH2], &currCH2[currPointerCH2], sizeof(uint32_t));
						}
		}
			currPointerCH1++;
			currPointerCH2++;
			}
	if (adc3result == 1){
		adc3result = 0;
		if(currPointerCH3 == ((sizeof(currCH3)/sizeof(float)) - 1)){
			currCH3[currPointerCH3] = RemapADC(dataADCCH3, 0, 500);

			if(currCH3[currPointerCH3] >= reg[LATCH_UP_TRSH_CH3]){
					ModuleResetSupply(3);
					reg[TEST_STAT] = 0;
					reg[WRITE_STAT] = 0;
					memcpy( &reg[LATCH_UP_EVENT_CH3], &currCH3[currPointerCH3], sizeof(uint32_t));
												}

			for(int i = 0; i < (sizeof(currCH3)/sizeof(float)); i++){
							sumCurrCH3 += currCH3[i];
						}



			sumCurrCH3 = sumCurrCH3/(sizeof(currCH3)/sizeof(float));
			memcpy( &reg[CURR_CH3_VAL], &sumCurrCH3, sizeof(uint32_t));
			currPointerCH3 = 0;

		}

		else{
			currCH3[currPointerCH3] = RemapADC(dataADCCH3, 0, 500);
			if(currCH3[currPointerCH3] >= reg[LATCH_UP_TRSH_CH3]){
				ModuleResetSupply(3);
				reg[TEST_STAT] = 0;
				reg[WRITE_STAT] = 0;
				memcpy( &reg[LATCH_UP_EVENT_CH3], &currCH3[currPointerCH3], sizeof(uint32_t));
												}
			currPointerCH3++;


		}

	}

}

void ModuleResetSupply(int channel){
	ModuleSetSupply(channel, 0);
	ModuleSetSupply(channel, 1);
}







