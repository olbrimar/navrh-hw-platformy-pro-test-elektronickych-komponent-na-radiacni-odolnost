#include "graph.h"
#include "qcustomplot.h"
#include <algorithm>

Graph::Graph(QWidget *parent) : QWidget(parent)
{
      plotMis = new QCustomPlot(this);
      plotCurr = new QCustomPlot(this);
      plotDelta = new QCustomPlot(this);
      plotMisECC = new QCustomPlot(this);

      /*Error graph*/

      plotMis->plotLayout()->insertRow(0);
      plotMis->plotLayout()->addElement(0, 0, new QCPTextElement(plotMis, "Number of errors", 14));
      plotMis->legend->setVisible(false);
      plotMis->yAxis->setLabel("number of errors [-]");

      misBar = new QCPBars(plotMis->xAxis, plotMis->yAxis);
      misBar->setAntialiased(false);

      /*XAxis*/
      xMisData << 1 << 2 << 3;
      xMisLabel << "channel 1"
                << "channel 2"
                << "channel 3 ";
      QSharedPointer<QCPAxisTickerText> textTicker(new QCPAxisTickerText);
      textTicker->addTicks(xMisData, xMisLabel);
      plotMis->xAxis->setRange(0, 4);
      plotMis->xAxis->setTicker(textTicker);
      plotMis->xAxis->setSubTicks(false);

      /*yAxis*/
      yMisData << 0 << 0 << 0;
      misBar->setData(xMisData, yMisData);
      plotMis->yAxis->setRange(-1, 1);

      /*ECC error graph*/

      plotMisECC->plotLayout()->insertRow(0);
      plotMisECC->plotLayout()->addElement(0, 0, new QCPTextElement(plotMisECC, "Cumulative ECC correction occurrence", 14));

      plotMisECC->legend->setVisible(false);
      plotMisECC->yAxis->setLabel("ECC correction occurrence [-]");

      misBarECC = new QCPBars(plotMisECC->xAxis, plotMisECC->yAxis);
      misBarECC->setAntialiased(false);

      /*XAxis*/
      xMisDataECC << 1 << 2 << 3;
      xMisLabelECC << "channel 1"
                   << "channel 2"
                   << "channel 3 ";
      QSharedPointer<QCPAxisTickerText> textTickerECC(new QCPAxisTickerText);
      textTickerECC->addTicks(xMisDataECC, xMisLabelECC);
      plotMisECC->xAxis->setRange(0, 4);
      plotMisECC->xAxis->setTicker(textTickerECC);
      plotMisECC->xAxis->setSubTicks(false);

      /*yAxis*/
      yMisDataECC << 0 << 0 << 0;
      misBarECC->setData(xMisDataECC, yMisDataECC);
      plotMisECC->yAxis->setRange(-1, 1);

      /*Plot graph with delta errors*/

      plotDelta->legend->setVisible(true);
      plotDelta->plotLayout()->insertRow(0);
      plotDelta->plotLayout()->addElement(0, 0, new QCPTextElement(plotDelta, "Delta of errors in time", 14));

      QCPLayoutGrid *subLayoutDelta = new QCPLayoutGrid;
      plotDelta->plotLayout()->addElement(1, 1, subLayoutDelta);
      subLayoutDelta->addElement(0, 0, plotDelta->legend);
      plotDelta->plotLayout()->setColumnStretchFactor(1, 0.0001);
      plotDelta->legend->setBorderPen(QPen(Qt::white));

      plotDelta->xAxis->setLabel("t [s]");
      plotDelta->yAxis->setLabel("delta of errors [-]");

      plotDelta->addGraph();
      plotDelta->graph(0)->setLineStyle(QCPGraph::lsImpulse);
      plotDelta->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 5));
      plotDelta->graph(0)->setName("Channel 1");
      plotDelta->graph(0)->setPen(QPen(Qt::blue));

      plotDelta->addGraph();
      plotDelta->graph(1)->setLineStyle(QCPGraph::lsImpulse);
      plotDelta->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 5));
      plotDelta->graph(1)->setName("Channel 2");
      plotDelta->graph(1)->setPen(QPen(Qt::red));

      plotDelta->addGraph();
      plotDelta->graph(2)->setLineStyle(QCPGraph::lsImpulse);
      plotDelta->graph(2)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 5));
      plotDelta->graph(2)->setName("Channel 3");
      plotDelta->graph(2)->setPen(QPen(Qt::green));

      /*Plot graph with current*/

      plotCurr->plotLayout()->insertRow(0);
      plotCurr->plotLayout()->addElement(0, 0, new QCPTextElement(plotCurr, "Current", 14));

      QCPLayoutGrid *subLayoutCurr = new QCPLayoutGrid;
      plotCurr->plotLayout()->addElement(1, 1, subLayoutCurr);
      subLayoutCurr->addElement(0, 0, plotCurr->legend);
      plotCurr->plotLayout()->setColumnStretchFactor(1, 0.0001);
      plotCurr->legend->setBorderPen(QPen(Qt::white));

      plotCurr->addGraph();
      plotCurr->graph(0)->setName("Channel 1");
      plotCurr->graph(0)->setPen(QPen(Qt::blue));

      plotCurr->addGraph();
      plotCurr->graph(1)->setName("Channel 2");
      plotCurr->graph(1)->setPen(QPen(Qt::red));

      plotCurr->addGraph();
      plotCurr->graph(2)->setName("Channel 3");
      plotCurr->graph(2)->setPen(QPen(Qt::green));

      plotCurr->addGraph();
      plotCurr->graph(3)->setName("Latch-up event");
      plotCurr->graph(3)->setLineStyle(QCPGraph::lsNone);
      plotCurr->graph(3)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 5));
      plotCurr->graph(3)->setPen(QPen(Qt::black));

      plotCurr->addGraph();
      plotCurr->graph(4)->setName("Latch-up event");
      plotCurr->graph(4)->setLineStyle(QCPGraph::lsNone);
      plotCurr->graph(4)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 5));
      plotCurr->graph(4)->setPen(QPen(Qt::black));

      plotCurr->addGraph();
      plotCurr->graph(5)->setName("Latch-up event");
      plotCurr->graph(5)->setLineStyle(QCPGraph::lsNone);
      plotCurr->graph(5)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 5));
      plotCurr->graph(5)->setPen(QPen(Qt::black));

      plotCurr->graph(4)->removeFromLegend();
      plotCurr->graph(5)->removeFromLegend();
      plotCurr->legend->setVisible(true);

      plotCurr->xAxis->setLabel("t [s]");
      plotCurr->yAxis->setLabel("I [mA]");
}

uint8_t Graph::draw_curr(uint32_t timeBase, uint32_t currCH1, uint32_t currCH2, uint32_t currCH3, uint32_t latchEventCH1, uint32_t latchEventCH2, uint32_t latchEventCH3)
{
      uint8_t retLatch = 0;
      xCurrData << timeBase;
      float value;
      memcpy(&value, &currCH1, sizeof(uint32_t));
      yCurrDataCH1.push_back((double)value);
      memcpy(&value, &currCH2, sizeof(uint32_t));
      yCurrDataCH2.push_back((double)value);
      memcpy(&value, &currCH3, sizeof(uint32_t));
      yCurrDataCH3.push_back((double)value);
      yLatchUpCH1.push_back(0);
      yLatchUpCH2.push_back(0);
      yLatchUpCH3.push_back(0);
      if (latchEventCH1 != 0)
      {
            memcpy(&yLatchUpCH1[yLatchUpCH1.size() - 1], &latchEventCH1, sizeof(uint32_t));
            retLatch = 1;
      }
      if (latchEventCH2 != 0)
      {
            memcpy(&yLatchUpCH2[yLatchUpCH2.size() - 1], &latchEventCH2, sizeof(uint32_t));
            retLatch = 1;
      }
      if (latchEventCH3 != 0)
      {
            memcpy(&yLatchUpCH3[yLatchUpCH3.size() - 1], &latchEventCH3, sizeof(uint32_t));
            retLatch = 1;
      }

      plotCurr->graph(0)->setData(xCurrData, yCurrDataCH1);
      plotCurr->graph(1)->setData(xCurrData, yCurrDataCH2);
      plotCurr->graph(2)->setData(xCurrData, yCurrDataCH3);
      plotCurr->graph(3)->setData(xCurrData, yLatchUpCH1);
      plotCurr->graph(4)->setData(xCurrData, yLatchUpCH2);
      plotCurr->graph(5)->setData(xCurrData, yLatchUpCH3);
      plotCurr->rescaleAxes();
      plotCurr->xAxis->setRange(0, timeBase + 1);
      plotCurr->replot();

      if (retLatch == 1)
            return 1;

      else
      {
            return 0;
      }
}

void Graph::draw_delta(uint32_t timeBase, uint32_t deltaCH1, uint32_t deltaCH2, uint32_t deltaCH3)
{
      xDeltaData.append((double)timeBase);
      yDeltaDataCH1.append((double)deltaCH1);
      yDeltaDataCH2.append((double)deltaCH2);
      yDeltaDataCH3.append((double)deltaCH3);
      plotDelta->graph(0)->setData(xDeltaData, yDeltaDataCH1);
      plotDelta->graph(1)->setData(xDeltaData, yDeltaDataCH2);
      plotDelta->graph(2)->setData(xDeltaData, yDeltaDataCH3);
      plotDelta->rescaleAxes();
      plotDelta->xAxis->setRange(0, timeBase + 1);
      plotDelta->replot();
}
