#include <QWidget>
#include "qcustomplot.h"
#include <QVector>

class Graph : public QWidget
{
    Q_OBJECT

public:
    Graph(QWidget *parent = 0);
    QCustomPlot *plotMis, *plotCurr, *plotDelta, *plotMisECC;
    QVector<double> xMisData, xMisDataECC, xDeltaData;
    QVector<double> yMisData, yMisDataECC, yDeltaDataCH1, yDeltaDataCH2, yDeltaDataCH3;
    QVector<double> xCurrData, yCurrDataCH1, yCurrDataCH2, yCurrDataCH3, yLatchUpCH1, yLatchUpCH2, yLatchUpCH3;
    QVector<QString> xMisLabel, xMisLabelECC;

    QCPBars *misBar, *misBarECC;
    uint8_t draw_curr(uint32_t timeBase, uint32_t currCH1, uint32_t currCH2, uint32_t currCH3, uint32_t latchEventCH1, uint32_t latchEventCH2, uint32_t latchEventCH3);
    void draw_delta(uint32_t timeBase, uint32_t deltaCH1, uint32_t deltaCH2, uint32_t deltaCH3);
};