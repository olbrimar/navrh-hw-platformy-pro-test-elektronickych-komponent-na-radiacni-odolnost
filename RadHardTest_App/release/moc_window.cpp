/****************************************************************************
** Meta object code from reading C++ file 'window.h'
**
** Created by: The Qt Meta Object Compiler version 68 (Qt 6.4.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../window.h"
#include <QtGui/qtextcursor.h>
#include <QScreen>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'window.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 68
#error "This file was generated using the moc from 6.4.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

#ifndef Q_CONSTINIT
#define Q_CONSTINIT
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
namespace {
struct qt_meta_stringdata_window_t {
    uint offsetsAndSizes[42];
    char stringdata0[7];
    char stringdata1[14];
    char stringdata2[1];
    char stringdata3[11];
    char stringdata4[15];
    char stringdata5[16];
    char stringdata6[16];
    char stringdata7[16];
    char stringdata8[15];
    char stringdata9[12];
    char stringdata10[21];
    char stringdata11[18];
    char stringdata12[16];
    char stringdata13[20];
    char stringdata14[19];
    char stringdata15[18];
    char stringdata16[20];
    char stringdata17[23];
    char stringdata18[17];
    char stringdata19[17];
    char stringdata20[17];
};
#define QT_MOC_LITERAL(ofs, len) \
    uint(sizeof(qt_meta_stringdata_window_t::offsetsAndSizes) + ofs), len 
Q_CONSTINIT static const qt_meta_stringdata_window_t qt_meta_stringdata_window = {
    {
        QT_MOC_LITERAL(0, 6),  // "window"
        QT_MOC_LITERAL(7, 13),  // "clickedConDis"
        QT_MOC_LITERAL(21, 0),  // ""
        QT_MOC_LITERAL(22, 10),  // "clickedRef"
        QT_MOC_LITERAL(33, 14),  // "changePushText"
        QT_MOC_LITERAL(48, 15),  // "stateChangedCH1"
        QT_MOC_LITERAL(64, 15),  // "stateChangedCH2"
        QT_MOC_LITERAL(80, 15),  // "stateChangedCH3"
        QT_MOC_LITERAL(96, 14),  // "stateChangedRW"
        QT_MOC_LITERAL(111, 11),  // "clickedSend"
        QT_MOC_LITERAL(123, 20),  // "clickedConstValWrite"
        QT_MOC_LITERAL(144, 17),  // "timerGraphTimeout"
        QT_MOC_LITERAL(162, 15),  // "clickedVarWrite"
        QT_MOC_LITERAL(178, 19),  // "clickedStartErrTest"
        QT_MOC_LITERAL(198, 18),  // "clickedStopErrTest"
        QT_MOC_LITERAL(217, 17),  // "timerWriteTimeout"
        QT_MOC_LITERAL(235, 19),  // "clickedFilenamePath"
        QT_MOC_LITERAL(255, 22),  // "clickedVarTestCheckBox"
        QT_MOC_LITERAL(278, 16),  // "sliderChangedCH1"
        QT_MOC_LITERAL(295, 16),  // "sliderChangedCH2"
        QT_MOC_LITERAL(312, 16)   // "sliderChangedCH3"
    },
    "window",
    "clickedConDis",
    "",
    "clickedRef",
    "changePushText",
    "stateChangedCH1",
    "stateChangedCH2",
    "stateChangedCH3",
    "stateChangedRW",
    "clickedSend",
    "clickedConstValWrite",
    "timerGraphTimeout",
    "clickedVarWrite",
    "clickedStartErrTest",
    "clickedStopErrTest",
    "timerWriteTimeout",
    "clickedFilenamePath",
    "clickedVarTestCheckBox",
    "sliderChangedCH1",
    "sliderChangedCH2",
    "sliderChangedCH3"
};
#undef QT_MOC_LITERAL
} // unnamed namespace

Q_CONSTINIT static const uint qt_meta_data_window[] = {

 // content:
      10,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags, initial metatype offsets
       1,    0,  128,    2, 0x0a,    1 /* Public */,
       3,    0,  129,    2, 0x0a,    2 /* Public */,
       4,    0,  130,    2, 0x0a,    3 /* Public */,
       5,    1,  131,    2, 0x0a,    4 /* Public */,
       6,    1,  134,    2, 0x0a,    6 /* Public */,
       7,    1,  137,    2, 0x0a,    8 /* Public */,
       8,    1,  140,    2, 0x0a,   10 /* Public */,
       9,    0,  143,    2, 0x0a,   12 /* Public */,
      10,    0,  144,    2, 0x0a,   13 /* Public */,
      11,    0,  145,    2, 0x0a,   14 /* Public */,
      12,    0,  146,    2, 0x0a,   15 /* Public */,
      13,    0,  147,    2, 0x0a,   16 /* Public */,
      14,    0,  148,    2, 0x0a,   17 /* Public */,
      15,    0,  149,    2, 0x0a,   18 /* Public */,
      16,    0,  150,    2, 0x0a,   19 /* Public */,
      17,    1,  151,    2, 0x0a,   20 /* Public */,
      18,    0,  154,    2, 0x0a,   22 /* Public */,
      19,    0,  155,    2, 0x0a,   23 /* Public */,
      20,    0,  156,    2, 0x0a,   24 /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

Q_CONSTINIT const QMetaObject window::staticMetaObject = { {
    QMetaObject::SuperData::link<QWidget::staticMetaObject>(),
    qt_meta_stringdata_window.offsetsAndSizes,
    qt_meta_data_window,
    qt_static_metacall,
    nullptr,
    qt_incomplete_metaTypeArray<qt_meta_stringdata_window_t,
        // Q_OBJECT / Q_GADGET
        QtPrivate::TypeAndForceComplete<window, std::true_type>,
        // method 'clickedConDis'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'clickedRef'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'changePushText'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'stateChangedCH1'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        QtPrivate::TypeAndForceComplete<bool, std::false_type>,
        // method 'stateChangedCH2'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        QtPrivate::TypeAndForceComplete<bool, std::false_type>,
        // method 'stateChangedCH3'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        QtPrivate::TypeAndForceComplete<bool, std::false_type>,
        // method 'stateChangedRW'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        QtPrivate::TypeAndForceComplete<bool, std::false_type>,
        // method 'clickedSend'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'clickedConstValWrite'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'timerGraphTimeout'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'clickedVarWrite'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'clickedStartErrTest'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'clickedStopErrTest'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'timerWriteTimeout'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'clickedFilenamePath'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'clickedVarTestCheckBox'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        QtPrivate::TypeAndForceComplete<bool, std::false_type>,
        // method 'sliderChangedCH1'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'sliderChangedCH2'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'sliderChangedCH3'
        QtPrivate::TypeAndForceComplete<void, std::false_type>
    >,
    nullptr
} };

void window::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<window *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->clickedConDis(); break;
        case 1: _t->clickedRef(); break;
        case 2: _t->changePushText(); break;
        case 3: _t->stateChangedCH1((*reinterpret_cast< std::add_pointer_t<bool>>(_a[1]))); break;
        case 4: _t->stateChangedCH2((*reinterpret_cast< std::add_pointer_t<bool>>(_a[1]))); break;
        case 5: _t->stateChangedCH3((*reinterpret_cast< std::add_pointer_t<bool>>(_a[1]))); break;
        case 6: _t->stateChangedRW((*reinterpret_cast< std::add_pointer_t<bool>>(_a[1]))); break;
        case 7: _t->clickedSend(); break;
        case 8: _t->clickedConstValWrite(); break;
        case 9: _t->timerGraphTimeout(); break;
        case 10: _t->clickedVarWrite(); break;
        case 11: _t->clickedStartErrTest(); break;
        case 12: _t->clickedStopErrTest(); break;
        case 13: _t->timerWriteTimeout(); break;
        case 14: _t->clickedFilenamePath(); break;
        case 15: _t->clickedVarTestCheckBox((*reinterpret_cast< std::add_pointer_t<bool>>(_a[1]))); break;
        case 16: _t->sliderChangedCH1(); break;
        case 17: _t->sliderChangedCH2(); break;
        case 18: _t->sliderChangedCH3(); break;
        default: ;
        }
    }
}

const QMetaObject *window::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *window::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_window.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int window::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType();
        _id -= 19;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
