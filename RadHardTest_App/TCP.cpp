#include "TCP.h"
#include <QDebug>
#include <string.h>

#define TEMP_VAL 0x00
#define CURR_CH1_VAL 0x01
#define CURR_CH2_VAL 0x02
#define CURR_CH3_VAL 0x03
#define SUPPLY_CH1 0x04
#define SUPPLY_CH2 0x05
#define SUPPLY_CH3 0x06
#define WRITE_STAT 0x07
#define TEST_STAT 0x08
#define MEAS_CH1_RES 0x9
#define MEAS_CH2_RES 0xA
#define MEAS_CH3_RES 0xB
#define NUMBER_OF_BITES 0xC
#define LATCH_UP_TRSH_CH1 0xD
#define LATCH_UP_TRSH_CH2 0xE
#define LATCH_UP_TRSH_CH3 0xF
#define LATCH_UP_EVENT_CH1 0x10
#define LATCH_UP_EVENT_CH2 0x11
#define LATCH_UP_EVENT_CH3 0x12
#define ECC_CORRECTION_CH1 0x13
#define ECC_CORRECTION_CH2 0x14
#define ECC_CORRECTION_CH3 0x15

#define CMD_SET_SUPPLY 0x80
#define CMD_WRITE_CONST 0x81
#define CMD_WRITE_VAR 0x82
#define CMD_START_TEST 0x83
#define CMD_STOP_TEST 0x84
#define CMD_NUMBER_OF_BYTES 0x85
#define CMD_LATCHUP_LEVEL 0x86

TCP::TCP(QWidget *parent) : QWidget(parent)
{
    socket = new QTcpSocket(this);
    connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
    connect(socket, SIGNAL(connected()), this, SLOT(connected()));
}

void TCP::connection(QString hostname, quint16 port)
{
    socket->connectToHost(hostname, port);
    qDebug() << "Connecting,..";
}

void TCP::connected()
{
    qDebug() << "Connected!" << socket->state();
}

void TCP::disconnected()
{
    qDebug() << "Disconnected!" << socket->state();
}

void TCP::readyRead()
{
    data = socket->readAll();
    QList<QByteArray> values = data.split('|');
    bool status = false;
    uint32_t addr = values.at(0).toUInt(&status, 16);
    uint32_t data = values.at(1).toUInt();
    reg[addr] = data;
}

uint32_t TCP::command_get(uint32_t addr)
{
    disconnect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
    socket->write("0x" + QByteArray::number(addr, 16));
    socket->waitForReadyRead();
    data = socket->readAll();
    QList<QByteArray> values = data.split('|');
    bool status = false;
    addr = values.at(0).toUInt(&status, 16);
    uint32_t data = values.at(1).toUInt();
    connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
    return data;
}

void TCP::command_set(uint32_t cmd, uint32_t channel, uint32_t data)
{
    QByteArray send = QByteArray::number(cmd, 16);
    send.append("|");
    send.append(QByteArray::number(channel));
    send.append("|");
    send.append(QByteArray::number(data, 16));
    send.append("|");
    socket->write(send);
    socket->waitForBytesWritten();
}

void TCP::loadReg()
{
    disconnect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
    for (int i = 0; i <= LATCH_UP_EVENT_CH3; i++)
    {
        socket->write("0x" + QByteArray::number(i, 16));
        socket->waitForReadyRead();
        data = socket->readAll();
        QList<QByteArray> values = data.split('|');
        bool status = false;
        uint32_t addr = values.at(0).toUInt(&status, 16);
        uint32_t data = values.at(1).toUInt();
        reg[addr] = data;
    }
    connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
}

void TCP::loadCurr()
{
    disconnect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
    for (int i = CURR_CH1_VAL; i <= CURR_CH3_VAL; i++)
    {
        socket->write("0x" + QByteArray::number(i, 16));
        socket->waitForReadyRead();
        data = socket->readAll();
        QList<QByteArray> values = data.split('|');
        bool status = false;
        uint32_t addr = values.at(0).toUInt(&status, 16);
        uint32_t data = values.at(1).toUInt();
        reg[addr] = data;
    }
    connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
}
