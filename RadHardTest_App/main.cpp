#include <QApplication>
#include <QWidget>


// #include "TCP.h"
#include "window.h"


int main(int argc, char *argv[])
{

  QApplication app(argc, argv);
  window w;
  w.setWindowTitle("Radiation hardness test");
  w.resize(1024, 768);
  w.show();
  return app.exec();
}
